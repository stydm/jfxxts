package com.jiufengxinxi.ts.init;

import com.jiufengxinxi.ts.web.controller.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author stydm
 * @className InitDevice
 * @description 设备初始化
 * @date 2020/7/9 10:59
 */

@Component
@Order(1)
public class InitDevice implements CommandLineRunner {

    private static final Logger logger =  LoggerFactory.getLogger(LoginController.class);

    @Override
    public void run(String... args) throws Exception {
        logger.info("!----------------- 设备开始初始化 -----------------!");
        logger.info("!----------------- 设备初始化成功 -----------------!");
    }
}
