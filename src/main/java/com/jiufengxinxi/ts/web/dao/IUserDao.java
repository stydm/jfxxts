package com.jiufengxinxi.ts.web.dao;

import com.jiufengxinxi.ts.web.bean.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @author stydm
 * @className IUserDao
 * @description userDao
 * @date 2020/7/8 17:12
 */

@Component
public interface IUserDao extends CrudRepository<User, Long> {

    public Optional<User> findById(Long id);

    public User save(User u);
}
