package com.jiufengxinxi.ts.web.controller;

import com.jiufengxinxi.ts.common.config.JfxxConfig;
import com.jiufengxinxi.ts.common.utils.JsonUtil;
import com.jiufengxinxi.ts.web.bean.User;
import com.jiufengxinxi.ts.web.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@RestController
public class LoginController {

    private static final Logger logger =  LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private JfxxConfig jfxxConfig;

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public String reg(@ModelAttribute User user) {

        userService.saveUser(user);

        User user1 = userService.getUser(1);

        return JsonUtil.toJson(user1);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute User user) {

        logger.info("你好啊");
        logger.debug("调试");
        logger.error("错误");
        logger.warn("警告");
        HashMap<String,Object> ss = new HashMap<>();
        ss.put("nihao",1);
        ss.put("ni",jfxxConfig.getTerminalKey());
        ss.put("hao",jfxxConfig.getTerminalVersion());
        if (user.getUsername().equals(jfxxConfig.getUsername())&&user.getPassword().equals(jfxxConfig.getPassword())) {
            return JsonUtil.toJson(ss);
        } else {
            return "2";
        }

    }
}
