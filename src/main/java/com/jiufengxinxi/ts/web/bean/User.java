package com.jiufengxinxi.ts.web.bean;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="user")
public class User {

    @Id
    private long id=1;

    @Column(columnDefinition = "text",name="username")
    private String username="jfxx";

    @Column(columnDefinition = "text",name="password")
    private String password="123456";

}
