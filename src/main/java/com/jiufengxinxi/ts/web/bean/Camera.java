package com.jiufengxinxi.ts.web.bean;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="camera")
public class Camera {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(columnDefinition = "text",name="server_ip")
    private String serverIp;

    @Column(columnDefinition = "text",name="server_port")
    private int serverPort;

    @Column(columnDefinition = "text",name="username")
    private String username;

    @Column(columnDefinition = "text",name="password")
    private String password;

}
