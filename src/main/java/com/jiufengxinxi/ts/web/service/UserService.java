package com.jiufengxinxi.ts.web.service;

import com.jiufengxinxi.ts.web.bean.User;
import com.jiufengxinxi.ts.web.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author stydm
 * @className UserService
 * @description TODO
 * @date 2020/7/8 17:23
 */

@Service
public class UserService {

    @Autowired IUserDao userDao;

    public User getUser(long id){
        return userDao.findById(id).get();
    }

    public void saveUser(User user){
        userDao.save(user);
    }
}
