package com.jiufengxinxi.ts.device.equipment.rfid;

import com.thingmagic.ReadExceptionListener;
import com.thingmagic.ReaderException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TagReadExceptionReceiver implements ReadExceptionListener
{
    String strDateFormat = "M/d/yyyy h:m:s a";
    SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
    public void tagReadException(com.thingmagic.Reader r, ReaderException re)
    {
        String format = sdf.format(Calendar.getInstance().getTime());
        System.out.println("Reader Exception: " + re.getMessage() + " Occured on :" + format);
        /*if(re.getMessage().equals("Connection Lost"))
        {
            System.exit(1);
        }*/
    }

}
