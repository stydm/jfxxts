package com.jiufengxinxi.ts.device.equipment;


import com.jiufengxinxi.ts.common.utils.CRCUtil;
import com.jiufengxinxi.ts.common.utils.HexUtil;
import com.jiufengxinxi.ts.common.utils.socket.ISocketRequestHandle;
import com.jiufengxinxi.ts.common.utils.socket.client.MultiThreadClient;
import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;
import com.jiufengxinxi.ts.device.interfaces.IDviceGeneral;
import org.apache.commons.lang3.StringUtils;

public class IO808Device extends IDviceGeneral {
	
	private MultiThreadClient client;
	
	private boolean reading=false;
	
	private Thread sendThread;
	
	public IO808Device(String ip,int port) {
		client = new MultiThreadClient(ip,port);
		client.setCharFilter(new IO88HexFilter());
	}
	
	public void setHandler(ISocketRequestHandle socketRequestHandle) {
		client.setSocketRequestHandle(socketRequestHandle);
	}
	
	
	public void readInIOSinalSend() {
		if(!reading) {
			
			sendThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					while(true) {
						try {
							//System.out.println("send : "+HexUtil.toBytes(IO88Sinal.SEND_READ_ALL_IN_IO));
							client.send(HexUtil.toBytes(IO88Sinal.SEND_READ_ALL_IN_IO));
							
							Thread.sleep(50);
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
					
				}
			});
			sendThread.start();
//			ExecutorServiceUtils.execute(new LoopThreadRunnable() {
//				
//				@Override
//				public boolean isUseLock() {
//					return false;
//				}
//				
//				@Override
//				public String getName() {
//					return "reading";
//				}
//				
//				@Override
//				public int sleepTime() {
//					return 50;
//				}
//				
//				@Override
//				public void loopElement() {
//					System.out.println("send : "+HexUtil.toBytes(IO88Sinal.SEND_READ_ALL_IN_IO));
//					client.send(HexUtil.toBytes(IO88Sinal.SEND_READ_ALL_IN_IO));
//				}
//				
//				@Override
//				public boolean endCondition() {
//					return false;
//				}
//			});
		}
		reading = true;
	}
	
	/**
	 * 断开一路IO
	 * @param index
	 */
	public void breakOne(int index) {
		String sinal = IO88Sinal.getSendBreakOneIo(coverage(index));
		client.send(HexUtil.toBytes(sinal));
	}
	
	/**
	 * 闭合一路IO
	 * @param index
	 */
	public void closeOne(int index) {
		String sinal = IO88Sinal.getSendCloseOneIo(coverage(index));
		client.send(HexUtil.toBytes(sinal));
	}
	
	
	/**
	 * 改变信号
	 * @param index
	 * @param sinal
	 */
	public void changeOne(int index,String sinal) {
		if(sinal.equals("00")) {
			breakOne(index);
		}else if(sinal.equals("01")){
			closeOne(index);
		}
	}
	
	
	private String coverage(int index) {
		String s = String.valueOf(index);
		if(s.length()==1) {
			s = "0"+s;
		}
		return s;
	}

	@Override
	public String status() {
		return check()?"connected":"disconnect";
	}

	@Override
	public boolean check() {
		return client.isConnected();
	}

	@Override
	public void destroy() {
		try {
			if(sendThread!=null) {
				sendThread.interrupt();
			}
			reading = false;
			client.disconnect();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void startup() {
		client.connection();
		//client.send(HexUtil.toBytes(password));
	}

	@Override
	public String getDeviceName() {
		return "IO88";
	}

	@Override
	public void initDevide(String... param) {
		startup();
	}

	public static class IO88Sinal{
		//断开一路IO
		private static final String SEND_BREAK_ONE_IO =      "110500";
		public static final String RECIVE_BREAK_ONE_IO =    "110500";
		
		//闭合一路IO
		private static final String SEND_CLOSE_ONE_IO =      "110500";
		public static final String RECIVE_CLOSE_ONE_IO =    "110500";
		
		
		//获取全部输入状态
		private static final String SEND_READ_ALL_IN_IO =    "1102002000087a96";
		public static final String RECIVE_READ_ALL_IN_IO =  "110201";
		
		public static String getSendBreakOneIo(String index) {
			String inio = SEND_BREAK_ONE_IO+index+"0000";
			String nx = CRCUtil.getCRC(HexUtil.hexStringToByte(inio));
			inio = inio + nx.substring(2, 4) +nx.substring(0, 2);
			return inio;
		}
		public static String getReciveBreakOneIo() {
			return RECIVE_BREAK_ONE_IO;
		}
		public static String getSendCloseOneIo(String index) {
			String inio = SEND_CLOSE_ONE_IO+index+"ff00";
			String nx = CRCUtil.getCRC(HexUtil.hexStringToByte(inio));
			inio = inio + nx.substring(2, 4) +nx.substring(0, 2);
			return inio;
		}
		public static String getReciveCloseOneIo() {
			return RECIVE_CLOSE_ONE_IO;
		}
		public static String getSendReadAllInIo() {
			return SEND_READ_ALL_IN_IO;
		}
		public static String getReciveReadAllInIo() {
			return RECIVE_READ_ALL_IN_IO;
		}
		
		
		
	}
	
	
	/**
	 * IO88专用编码指令过滤
	 * @author heshuangfeng
	 *
	 */
	public class IO88HexFilter extends MultiThreadClient.HexFilter{
		
		private int bitsit = 8;
		
		@Override
		public String change(byte[] bs) {
			String hex = "";
			//System.out.println(hex);
			
			try {
				hex = HexUtil.bytesToHexFun3(bs);
				if(hex.startsWith(IO88Sinal.RECIVE_READ_ALL_IN_IO)) {
					hex = hex.substring(6,8);
				}else {
					hex = "";
				}
				
			} catch (Exception e) {
				hex = "";
			}
			
			if(StringUtils.isNoneEmpty(hex)) {
				hex = new StringBuffer(HexUtil.hexString2binaryString(hex)).reverse().toString();
			}
			
			return hex;
		}

		public int getBitsit() {
			return bitsit;
		}

		public void setBitsit(int bitsit) {
			this.bitsit = bitsit;
		}
		
	}
	
	
	public static void main(String[] args) {
		String n = "admin\\r\\n";
		n = n.replace("\\r", "\r").replace("\\n", "\n");
		System.out.println(n+"   "+HexUtil.toHex(n));
	}

	@Override
	public void setDeviceCallback(IDeviceStateCallback deviceCallback) {
		// TODO Auto-generated method stub
		
	}
	
}
