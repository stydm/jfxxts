package com.jiufengxinxi.ts.device.equipment.printer;

import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

public class T500PrinterDevice extends IPrinterDevice {



    public T500PrinterDevice(String pid,String vid){
        super(Short.parseShort(pid.replace("0x",""),16),Short.parseShort(vid.replace("0x",""),16));
    }

    public T500PrinterDevice(String port){
        super(port);
    }

    public T500PrinterDevice(String host, int port) {
        super(host,port);
    }


    @Override
    public String getDeviceName() {
        return super.getDeviceName();
    }

    @Override
    public void setDeviceCallback(IDeviceStateCallback deviceCallback) {
        super.setDeviceCallback(deviceCallback);
    }

    @Override
    protected void excutePrint(String text) {
        try {

            write(T500PrinterAdapter.init_print());
            write(T500PrinterAdapter.printer_state(4));
            if (text.contains("No:")||text.contains("No：")) {
                String qrcode = text.substring(text.indexOf("No")+3,text.indexOf("\n",text.indexOf("No:")));
                write(T500PrinterAdapter.align_print(2));
                write(T500PrinterAdapter.qrcode_size(6));
                write(T500PrinterAdapter.qrcode_w_level(32));
                write(T500PrinterAdapter.qrcode_set(qrcode));
                write(T500PrinterAdapter.qrcode_print());
                //write(T500PrinterAdapter.print_paper_out(120));
            }

            write(T500PrinterAdapter.print_width(128,2));
            write(T500PrinterAdapter.align_print(0));
            write(T500PrinterAdapter.intl_char_set(0));
            write(T500PrinterAdapter.chinese_model_set());
            write(T500PrinterAdapter.character_pitch_right(2));
            write(T500PrinterAdapter.row_spacing(30));
            write(T500PrinterAdapter.characterChineseSet(0, 0, 0, 0));
            //connectUsbEqu.sendMassge(T500PrinterAdapter.font_type(1));
            write(T500PrinterAdapter.characterWestSet(0, 0, 1, 0, 0));
            write(("\n" + text).getBytes("GBK"));
            //connectUsbEqu.sendMassge(T500PrinterAdapter.print_line_feed());

            //print_line_feed()
            write(T500PrinterAdapter.print_line_feed());
            write(T500PrinterAdapter.print_line_feed());
            write(T500PrinterAdapter.print_line_feed());
            write(T500PrinterAdapter.print_line_feed());
            write(T500PrinterAdapter.print_line_feed());
            write(T500PrinterAdapter.print_line_feed());
            write(T500PrinterAdapter.cut_half());
//            write(T500PrinterAdapter.print_line_feed());
//            write(T500PrinterAdapter.print_line_feed());
            //write(T500PrinterAdapter.cut_half());
        }catch (Exception e){
            logger.error("打印失败："+e.getMessage(),e);
        }
    }


    public static void main(String[] args){

        //IPrinterDevice printer = new T500PrinterDevice("5812","28e9");
        //printer.print("No：11670208\n类型:转栈作业\n车号:粤P00492\n船名:丽贝乐\n堆位:06位-40~150米\n出库单号:\n地磅单号:BDB041902665\n货主:\n转入堆位:3W门21\n空重（吨）:15.28\n毛重（吨）:32.94\n净重（吨）:17.66\n累计重量(吨):1129.62车次：57\n司磅员:蓝飞柏\n作业票号:BGB041904182000\n日期:2019-04-18日班\n过磅时间:17:41:35\n新沙港务有限公司地中衡记录(重)\nuuid：bf299c2b-0673-4b58-b583-eab6179934ab");
        ThreadPoolTaskExecutor poolTaskExecutor=new ThreadPoolTaskExecutor();
        poolTaskExecutor.initialize();
        poolTaskExecutor.setCorePoolSize(6);
        poolTaskExecutor.setKeepAliveSeconds(200);
        poolTaskExecutor.setMaxPoolSize(10);
        poolTaskExecutor.setQueueCapacity(300);
        poolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        poolTaskExecutor.initialize();
//        ExecutorServiceUtils.executorService.setPoolTaskExecutor(poolTaskExecutor);


        T500PrinterDevice printerDevice = new T500PrinterDevice("10.197.32.229",9100);
        printerDevice.initDevide();
        printerDevice.print("类型:转栈作业\\n车号:粤P00492\\n船名:丽贝乐\\n堆位:06位-40~150米\\n出库单号:\\n地磅单号:BDB041902665\\n货主:\\n转入堆位:3W门21\\n空重（吨）:15.28\\n毛重（吨）:32.94\\n净重（吨）:17.66\\n累计重量(吨):1129.62车次：57\\n司磅员:蓝飞柏\\n作业票号:BGB041904182000\\n日期:2019-04-18日班\\n过磅时间:17:41:35\\n新沙港务有限公司地中衡记录(重)\\nuuid：bf299c2b-0673-4b58-b583-eab6179934ab");

//        PrinterCfg cfg = new PrinterCfg();
//        cfg.setType(2);
//        cfg.setBlandmodel("0");
//        cfg.setHost("10.197.32.229");
//        cfg.setPort("515");
//        cfg.setCode("0");
////        cfg.setPid("0x5812");
////        cfg.setVid("0x28e9");
//
//        PrinterBusinessDevice dev = new PrinterBusinessDevice(cfg);
//        dev.init();
//        dev.call(Constants.InOutType.STRING,"2No：11670208\n类型:转栈作业\n车号:粤P00492\n船名:丽贝乐\n堆位:06位-40~150米\n出库单号:\n地磅单号:BDB041902665\n货主:\n转入堆位:3W门21\n空重（吨）:15.28\n毛重（吨）:32.94\n净重（吨）:17.66\n累计重量(吨):1129.62车次：57\n司磅员:蓝飞柏\n作业票号:BGB041904182000\n日期:2019-04-18日班\n过磅时间:17:41:35\n新沙港务有限公司地中衡记录(重)\nuuid：bf299c2b-0673-4b58-b583-eab6179934ab");


        /*ConnectUsbEqu connectUsbEqu= new ConnectUsbEqu(Short.parseShort("5812",16),Short.parseShort("28e9",16));
        try {
            connectUsbEqu.sendMassge(T500PrinterAdapter.init_print());
            connectUsbEqu.sendMassge(T500PrinterAdapter.align_print(2));
            connectUsbEqu.sendMassge(T500PrinterAdapter.qrcode_size(6));
            connectUsbEqu.sendMassge(T500PrinterAdapter.qrcode_w_level(32));
            connectUsbEqu.sendMassge(T500PrinterAdapter.qrcode_set("11670208"));
            connectUsbEqu.sendMassge(T500PrinterAdapter.qrcode_print());
            //connectUsbEqu.sendMassge(T500PrinterAdapter.print_line_feed());
            //connectUsbEqu.sendMassge(T500PrinterAdapter.cut_half());
            connectUsbEqu.sendMassge(T500PrinterAdapter.print_paper_out(150));

            connectUsbEqu.sendMassge(T500PrinterAdapter.align_print(0));
            connectUsbEqu.sendMassge(T500PrinterAdapter.intl_char_set(0));
            connectUsbEqu.sendMassge(T500PrinterAdapter.chinese_model_set());
            connectUsbEqu.sendMassge(T500PrinterAdapter.character_pitch_right(2));
            connectUsbEqu.sendMassge(T500PrinterAdapter.row_spacing(40));
            connectUsbEqu.sendMassge(T500PrinterAdapter.characterChineseSet(0,0,0,0));
            //connectUsbEqu.sendMassge(T500PrinterAdapter.font_type(1));
            connectUsbEqu.sendMassge(T500PrinterAdapter.characterWestSet(0,0,1,0,0));
            connectUsbEqu.sendMassge("\nNo：11670208\n类型:转栈作业\n车号:粤P00492\n船名:丽贝乐\n堆位:06位-40~150米\n出库单号:\n地磅单号:BDB041902665\n货主:\n转入堆位:3W门21\n空重（吨）:15.28\n毛重（吨）:32.94\n净重（吨）:17.66\n累计重量(吨):1129.62车次：57\n司磅员:蓝飞柏\n作业票号:BGB041904182000\n日期:2019-04-18日班\n过磅时间:17:41:35\n新沙港务有限公司地中衡记录(重)\nuuid：bf299c2b-0673-4b58-b583-eab6179934ab".getBytes("GBK"));
            //connectUsbEqu.sendMassge(T500PrinterAdapter.print_line_feed());
            connectUsbEqu.sendMassge(T500PrinterAdapter.cut_all());
            connectUsbEqu.sendMassge(T500PrinterAdapter.printer_state(4));
            connectUsbEqu.sendMassge(T500PrinterAdapter.transer_state());


            //System.out.println(connectUsbEqu.receivedData());
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public String request(String report, String from) {

        System.out.println("return ==============================:"+report);
        return null;
    }




}
