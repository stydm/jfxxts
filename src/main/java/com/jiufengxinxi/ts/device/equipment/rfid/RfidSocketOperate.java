package com.jiufengxinxi.ts.device.equipment.rfid;

import com.jiufengxinxi.ts.common.utils.socket.ISocketRequestHandle;
import com.jiufengxinxi.ts.common.utils.socket.client.MultiThreadClient;
import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;
import com.jiufengxinxi.ts.device.interfaces.IReadDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Component
public class RfidSocketOperate implements IReadDevice, ISocketRequestHandle {
	
	private MultiThreadClient client;
	
	private Logger logger= LoggerFactory.getLogger(this.getClass());

	private String readerURI;
	private int[] antennaList;
	
	private String[] antennaGroup;
	
	private String antennas;
	
	private IReadListener readListener;
	
	private int state=0;//0=未初始,1=已经初始，2=监听读卡中,3=读卡中,4=写卡中
	
	private int readType=0;//0=EPC,1=TID
	
	private int power=3150;
	
	private String strDateFormat = "M/d/yyyy h:m:s a";
	
	private SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
	
	public void init(String readerURI, String[] antennaGroup,int readerPower) {
		this.readerURI = readerURI.replace("socket://", "");
		this.power=readerPower;
		this.antennaGroup=antennaGroup;
		if(antennaGroup==null) {
			return ;
		}
		
		this.antennas = "";
		
		List<Integer> antennas=new ArrayList<Integer>();
		for(String as:antennaGroup){
			String[] at=as.split(",");
			for(String a:at){
				antennas.add(Integer.parseInt(a));
				this.antennas+=","+a;
			}
		}
		
		this.antennas = this.antennas.substring(1);
		
		this.antennaList=new int[antennas.size()];
		for(int i=0;i<antennaList.length;i++){
			antennaList[i]=antennas.get(i);
		}
		
		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void init() throws Exception {
		if(client==null) {
			client = new MultiThreadClient("10.197.32.176" , 50007);
			client.connection();
		}
		
		client.send(String.format("INIT|%s|%s&&&&", this.readerURI, this.antennas));
		
	}

	@Override
	public void listen() throws Exception {
		client.send("OPEN&&&&");
	}

	@Override
	public void stopListen() {
		client.send("CLOSE&&&&");
	}

	@Override
	public String status() {
		return client.isConnected()+"";
	}

	@Override
	public boolean check() {
		return client.isConnected();
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void startup() {
		
	}

	@Override
	public String getDeviceName() {
		return "M6eRead";
	}

	@Override
	public void setDeviceCallback(IDeviceStateCallback deviceCallback) {
		
	}

	


	@Override
	public String write(String tag, String targetTag, int antenna) {
		return null;
	}

	@Override
	public List<String> read() throws Exception {
		return null;
	}

	@Override
	public boolean writePower(int readPower) {
		client.send(String.format("POW|%d&&&&", readPower));
		return true;
	}

	public int getReadType() {
		return readType;
	}

	public void setReadType(int readType) {
		this.readType = readType;
	}

	@Override
	public int getState() {
		return state;
	}

	@Override
	public String request(String report, String from) {
		if(report.contains("&&&&")) {
			String[] rps = report.split("&&&&");
			for(String rp : rps) {
				request(rp, from);
			}
			return null;
		}
		
		System.out.println(report);
		
		if(report.contains("RES|INIT")) {
			state = 1;
		}
		
		if(report.contains("RES|OPEN")) {
			state = 2;
		}
		
		if(report.contains("ERROR")) {
			client.send("RESET&&&&");
			state = 0;
			try {
				init();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(report.startsWith("RFID")) {
			report = report.replace("RFID|", "").replace("|&&&&", "").replace("&&&&", "");
			
			String rfid = report.substring(report.indexOf("(")+1, report.indexOf(")"));
			String[] rfs = report.split(",");
			int rssi = Integer.parseInt(rfs[2]);
			int ant = Integer.parseInt(rfs[3]);
			
			String[] cn = rfid.split(",");
			if(readListener!=null) {
				readListener.tag(cn[0], cn[1], ant, rssi);
			}
		}
		
		return null;
	}

	@Override
	public boolean decodeRelation(String report) {
		return false;
	}


	public String getReaderURI() {
		return readerURI;
	}


	public void setReaderURI(String readerURI) {
		this.readerURI = readerURI;
	}


	public int[] getAntennaList() {
		return antennaList;
	}


	public void setAntennaList(int[] antennaList) {
		this.antennaList = antennaList;
	}
	
	public IReadListener getReadListener() {
		return readListener;
	}

	public void setReadListener(IReadListener readListener) {
		this.readListener = readListener;
	}


	@Override
	public void connection(String from) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String disconnection(String from) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
