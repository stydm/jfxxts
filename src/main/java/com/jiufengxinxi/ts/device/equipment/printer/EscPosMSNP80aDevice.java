package com.jiufengxinxi.ts.device.equipment.printer;


import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;

import java.io.UnsupportedEncodingException;

public class EscPosMSNP80aDevice extends IPrinterDevice{


    public EscPosMSNP80aDevice(String pid,String vid) {
        super(Short.parseShort(pid.replace("0x",""),16),Short.parseShort(vid.replace("0x",""),16));
    }

    public EscPosMSNP80aDevice(String host, int port) {
        super(host,port);
    }

    public boolean isUsb(){
        return getPid()!=0&&getVid()!=0;
    }

    public EscPosMSNP80aDevice(String port) {
        super(port);
        setCoded("GBK");
        setBaudrate(115200);
        setReciveBytes(true);
        startup();
    }

    @Override
    public String getDeviceName() {
        return "MS_NP80A";
    }

    @Override
    public void setDeviceCallback(IDeviceStateCallback deviceCallback) {
        super.setDeviceCallback(deviceCallback);
    }

    public void excutePrint(String text){
        write(EscPosMSNP80aAdapter.init());
//        write(EscPosMSNP80aAdapter.printer_state(1));
//        write(EscPosMSNP80aAdapter.printer_state(2));
//        write(EscPosMSNP80aAdapter.printer_state(3));
        write(EscPosMSNP80aAdapter.printer_state(4));
//        write(EscPosMSNP80aAdapter.printer_state(5));
//        write(EscPosMSNP80aAdapter.transer_state());
        //System.out.println("MS-print:"+text);
        if (text.contains("No:")||text.contains("No：")) {
            String qrcode = text.substring(text.indexOf("No")+3,text.indexOf("\n",text.indexOf("No:")));
            //text=text.substring(qrcode.length()+3);
            //write(EscPosMSNP80aAdapter.print_paper_skip_n(50));
            write(EscPosMSNP80aAdapter.margin_left_set(410));
            write(EscPosMSNP80aAdapter.qrcode_set(3,5,null));
            write(EscPosMSNP80aAdapter.qrcode_set(4,1,qrcode));
            write(EscPosMSNP80aAdapter.qrcode_set(5,0,null));
//            write(EscPosMSNP80aAdapter.print_paper_out(80));
//            write(EscPosMSNP80aAdapter.print_paper_out(50));

        }

        write(EscPosMSNP80aAdapter.margin_left_set(0));
        write(EscPosMSNP80aAdapter.composing_set(30));
        write(EscPosMSNP80aAdapter.intl_char_set(0));
        write(EscPosMSNP80aAdapter.characterWestSet(0,0,1,0,0));
        write(EscPosMSNP80aAdapter.characterChineseSet(0,0,0,0));
        write(EscPosMSNP80aAdapter.chinese_model_set());
        try {
            write(text.getBytes("GBK"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        write(EscPosMSNP80aAdapter.print_paper_skip_n(170));
        write(EscPosMSNP80aAdapter.cut_half());
    }

    public void out_pp(int num){
        write(EscPosMSNP80aAdapter.init());
        write(EscPosMSNP80aAdapter.print_paper_skip_n(num));
    }

    @Override
    public String request(String report, String from) {
        return null;
    }
}
