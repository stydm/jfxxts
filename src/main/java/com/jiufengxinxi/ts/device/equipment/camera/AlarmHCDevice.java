package com.jiufengxinxi.ts.device.equipment.camera;


import com.jiufengxinxi.ts.device.interfaces.base.HCNetSDK;
import com.jiufengxinxi.ts.device.interfaces.base.HCNetSDKUtill;
import com.jiufengxinxi.ts.web.bean.Camera;
import com.sun.jna.NativeLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * 海康威视 摄像头设备
 */
public class AlarmHCDevice {

    private static final Logger logger =  LoggerFactory.getLogger(AlarmHCDevice.class);



    private Camera camera = null;

    private NativeLong loguserId;


    /**
     * 0 抓拍
     * 1 车牌识别 布防
     */
    private int handleType;

    /**
     * 布防 ID
     */
    private NativeLong ihandle;

    /**
     * 当前用户信息
     */
    private HCNetSDK.NET_DVR_DEVICEINFO_V30 struDeviceInfo;

    public AlarmHCDevice(Camera camera) {
        this.camera = camera;
        this.handleType = 1;
    }

    public void init(){
        HCNetSDKUtill.INIT_SDK();
    }

    public void start(){
        Assert.notNull(this.camera.getServerIp(),"布防IP不能为空");
        Assert.notNull(this.camera.getServerPort(),"布防端口不能为空");
        Assert.notNull(this.camera.getUsername(),"布防用户不能为空");
        Assert.notNull(this.camera.getPassword(),"布防密码不能为空");


        HCNetSDK.NET_DVR_DEVICEINFO_V30 struDeviceInfo = new HCNetSDK.NET_DVR_DEVICEINFO_V30();

        int num = 0;

        Long lUserID = null;

        while((ihandle==null||ihandle.intValue()<0||loguserId==null||loguserId.intValue()<0)&&num++<5) {
            logger.info(this.camera.getServerIp()+"布防设备 开始登录用户");
            loguserId = HCNetSDKUtill.hCNetSDK.NET_DVR_Login_V30(this.camera.getServerIp(),
                    (short) this.camera.getServerPort(),this.camera.getUsername(),this.camera.getPassword(),struDeviceInfo);
            lUserID = loguserId.longValue();
            if (lUserID < 0){
                int ierr = HCNetSDKUtill.hCNetSDK.NET_DVR_GetLastError();
                //hCNetSDK.NET_DVR_Cleanup();
                logger.info(this.camera.getServerIp()+"布防设备 登录失败 "+ierr);
                return;
            }else{
                struDeviceInfo.read();
                logger.info(this.camera.getServerIp()+"布防设备 登录成功");

                //return;
            }

            switch (handleType) {
                case 0: {

                }
                break;
                case 1: {
                    //HCNetSDKUtill.hCNetSDK.NET_DVR_SetDVRMessageCallBack_V30(alarmDeviceCallback,null);
                    HCNetSDK.NET_DVR_SETUPALARM_PARAM dvr_setupalarm_param = new HCNetSDK.NET_DVR_SETUPALARM_PARAM();
                    dvr_setupalarm_param.byAlarmInfoType = 1;//上传报警信息类型: 0- 老报警信息(NET_DVR_PLATE_RESULT), 1- 新报警信息(NET_ITS_PLATE_RESULT)
                    dvr_setupalarm_param.dwSize = 0;

                    ihandle = HCNetSDKUtill.hCNetSDK.NET_DVR_SetupAlarmChan_V41(loguserId, dvr_setupalarm_param);
                    if (ihandle.intValue() < 0) {
                        logger.error(this.camera.getServerIp() + " 布防失败：" + HCNetSDKUtill.hCNetSDK.NET_DVR_GetLastError());
                        HCNetSDKUtill.hCNetSDK.NET_DVR_Logout(loguserId);
                    }
                }
                break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void stop(){
        if(!HCNetSDKUtill.hCNetSDK.NET_DVR_CloseAlarmChan_V30(ihandle)){
            logger.error(this.camera.getServerIp()+"撤防失败："+HCNetSDKUtill.hCNetSDK.NET_DVR_GetLastError());
        }

        if(loguserId!=null) {
            HCNetSDKUtill.hCNetSDK.NET_DVR_Logout(loguserId);
        }
        //hCNetSDK.NET_DVR_Cleanup();
    }


    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public NativeLong getLoguserId() {
        return loguserId;
    }

    public void setLoguserId(NativeLong loguserId) {
        this.loguserId = loguserId;
    }

    public int getHandleType() {
        return handleType;
    }

    public void setHandleType(int handleType) {
        this.handleType = handleType;
    }

    public NativeLong getIhandle() {
        return ihandle;
    }

    public void setIhandle(NativeLong ihandle) {
        this.ihandle = ihandle;
    }

    public HCNetSDK.NET_DVR_DEVICEINFO_V30 getStruDeviceInfo() {
        return struDeviceInfo;
    }

    public void setStruDeviceInfo(HCNetSDK.NET_DVR_DEVICEINFO_V30 struDeviceInfo) {
        this.struDeviceInfo = struDeviceInfo;
    }
}
