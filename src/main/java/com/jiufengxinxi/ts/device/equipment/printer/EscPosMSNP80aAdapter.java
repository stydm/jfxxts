package com.jiufengxinxi.ts.device.equipment.printer;


import com.jiufengxinxi.ts.common.utils.HexUtil;

public class EscPosMSNP80aAdapter {

    public static final int ESC = 27;// 换码
    public static final int FS = 28;// 文本分隔符
    public static final int GS = 29;// 组分隔符
    public static final int DLE = 16;// 数据连接换码
    public static final int EOT = 4;// 传输结束
    public static final int ENQ = 5;// 询问字符
    public static final int SP = 32;// 空格
    public static final int HT = 9;// 横向列表
    public static final int LF = 10;// 打印并换行（水平定位）
    public static final int CR = 13;// 归位键
    public static final int FF = 12;// 走纸控制（打印并回到标准模式（在页模式下） ）
    public static final int CAN = 24;// 作废（页模式下取消打印数据 ）
    public static final int DC3 = 19;// 作废（页模式下取消打印数据 ）


    /**
     * 初始化
     * @return
     */
    public static byte[] init(){
        return new byte[]{ESC,64};
    }

    /**
     * 打印并走纸到下页
     * @return
     */
    public static byte[] print_paper_skip(){
        return new byte[]{GS,12};
    }

    /**
     * 打印并换行
     * @return
     */
    public static byte[] print_line_feed(){
        return new byte[]{LF};
    }

    /**
     * 打印并进纸
     * @return
     */
    public static byte[] print_paper_skip_n(int n){
        return new byte[]{ESC,74,(byte)n};
    }

    /**
     * 打印并退纸
     * @return
     */
    public static byte[] print_paper_out(int n){
        return new byte[]{ESC,75,(byte)n};
    }

    /**
     * 选择西文字符打印模式
     * @param size  0 24*24  ;  1 16*16
     * @param strong  1 加粗
     * @param dbw   1 双倍宽
     * @param dbh   1 双倍高
     * @param underline     1 下划线
     * @return
     */
    public static byte[] characterWestSet(int size,int strong,int dbw,int dbh,int underline){
        String s = underline+"0"+dbw+dbh+strong+"00"+size;
        return new byte[]{ESC,33,(byte) HexUtil.binaryToDecimal(s)};
    }

    /**
     * 选择汉字符打印模式
     * @param size  0 24*24  ;  1 16*16
     * @param dbw   1 双倍宽
     * @param dbh   1 双倍高
     * @param underline     1 下划线
     * @return
     */
    public static byte[] characterChineseSet(int size,int dbw,int dbh,int underline){
        String s = underline+"000"+dbh+dbw+"0"+size;
        return new byte[]{FS,33,(byte) HexUtil.binaryToDecimal(s)};
    }

    /**
     * 选择字符大小
     * @param n
     * @return
     */
    public static byte[] characterSizeSet(int n){
        byte realSize = 0;
        switch (n) {
            case 1:
                realSize = 0;
                break;
            case 2:
                realSize = 17;
                break;
            case 3:
                realSize = 42;
                break;
            case 4:
                realSize = 51;
                break;
            case 5:
                realSize = 68;
                break;
            case 6:
                realSize = 85;
                break;
            case 7:
                realSize = 102;
                break;
            case 8:
                realSize = 119;
                break;
        }
        return new byte[]{FS,33,realSize};
    }

    /**
     * 设置/取消下划线
     * 0
     * 1
     * 2
     * @return
     */
    public static byte[] under_line_set(int n){
        return new byte[]{ESC,45,(byte)n};
    }

    /**
     * 设定/解除粗体打印
     * 0
     * 1
     * 2
     * @return
     */
    public static byte[] strong_set(int n){
        return new byte[]{ESC,69,(byte)n};
    }

    /**
     * 设定/解除反白打印
     * 0
     * 1
     * @return
     */
    public static byte[] reverse_white_set(int n){
        return new byte[]{GS,66,(byte)n};
    }

    /**
     * 设定/取消字符旋转
     * 0
     * 1
     * @return
     */
    public static byte[] rotate_set(int n){
        return new byte[]{ESC,56,(byte)n};
    }

    /**
     * 设定汉字打印模式
     * @return
     */
    public static byte[] chinese_model_set(){
        return new byte[]{FS,38};
    }

    /**
     * 设定汉字打印模式
     * @return
     */
    public static byte[] chinese_ascii_set(){
        return new byte[]{FS,46};
    }

    /**
     * 选择国际字符集
     * n	字符集
     * 0	美国
     * 1	法国
     * 2	德国
     * 3	英国
     * 4	丹麦 I
     * 5	瑞典
     * 6	意大利
     * 7	西班牙 I
     * 8	日本
     * 9	挪威
     * 10	丹麦 II
     * @return
     */
    public static byte[] intl_char_set(int n){
        return new byte[]{ESC,82,(byte)n};
    }


    /**
     *编号	字符页	编号	字符页
     * 0	PC437[美国欧洲标准]	14	PC1251
     * 1	PC737	15	PC1252
     * 2	PC775	16	PC1253
     * 3	PC850	17	PC1254
     * 4	PC852	18	PC1255
     * 5	PC855	19	PC1256
     * 6	PC857	20	PC1257
     * 7	PC858	21	PC928
     * 8	PC860	22	Hebrew old
     * 9	PC862	23	IINTEL CHAR
     * 10	PC863	24	Katakana
     * 11	PC864	25	特殊符号 00-1F
     * 12	PC865	26	SPACE PAGE
     * 13	PC866
     * @param n
     * @return
     */
    public static byte[] page_char_set(int n){
        return new byte[]{ESC,116,(byte)n};
    }


    /**
     * 设置行间距
     * @param n
     * @return
     */
    public static byte[] composing_set(int n){
        return new byte[]{ESC,51,(byte)n};
    }


    /**
     *设置字符右间距
     * @param n
     * @return
     */
    public static byte[] margin_right_set(int n){
        return new byte[]{ESC,32,(byte)n};
    }

    /**
     *选择对齐方式
     * @param n
     * @return
     */
    public static byte[] align_set(int n){
        return new byte[]{ESC,97,(byte)n};
    }


    /**
     * 设置左边距
     * @param width
     * @return
     */
    public static byte[] margin_left_set(int width){
        return new byte[]{GS,76,(byte)(width%256),(byte)(width/256)};
    }

    /**
     * 设置打印区域宽度
     * @param nL
     * @param nH
     * @return
     */
    public static byte[] print_area_set(int nL,int nH){
        return new byte[]{GS,87,(byte)nL,(byte)nH};
    }

    /**
     *设置条形码高度
     * @param n
     * @return
     */
    public static byte[] barcode_h_set(int n){
        return new byte[]{GS,104,(byte)n};
    }

    /**
     *设置条形码宽度
     * @param n
     * @return
     */
    public static byte[] barcode_w_set(int n){
        return new byte[]{GS,119,(byte)n};
    }

    /**
     *选择条码可识读字符的打印位置
     * @param n
     * @return
     */
    public static byte[] barcode_h_dectc_set(int n){
        return new byte[]{GS,72,(byte)n};
    }

    /**
     *选择条码可识读字符的打印位置
     * @param n
     * @return
     */
    public static byte[] barcode_align_set(int n){
        return new byte[]{GS,90,(byte)n};
    }

    /**
     * 打印条码
     * @param s
     * @return
     */
    public static byte[] barcode_align_set(int m,String s){
        byte[] barcodebytes = s.getBytes();
        byte[] bts = new byte[2+barcodebytes.length];
        bts[0] = GS;
        bts[1] = 107;
        bts[2] = (byte)m;
        int idx = 3;
        for ( int i = 0; i < barcodebytes.length; i++ ){
            bts[idx] = barcodebytes[i];
            idx++;
        }
        bts[idx] = 0;
        return bts;
    }


    /**
     * 二维码打印
     * n=1 时，m 为设置二维码的版本号，取值范围为（1-40）；
     * n=2 时，m 为设置二维码的纠错等级,取值范围为(1-4)；
     * n=3 时，m 为设置二维码模块宽度,取值范围为(1-9)；
     * n=4 时，m 为设置二维码的内容； 格式 1: m + DH+DL + 数据内容(内码)
     *      格式 1: m + DH+DL + 数据内容(内码) m =01    DH*256 +DL ：字节长度
     *      格式 2: m +数据内容(内码)+ 00   m =02
     * n=5 时，直接打印二维码。
     * @return
     */
    public static byte[] qrcode_set(int n,int m,String content){
        byte[] bts = content==null?null:content.getBytes();
        byte[] bytes = null;
        switch(n){
            case 1:
            case 2:
            case 3: bytes = new byte[]{DC3,80,72,(byte)n,(byte)m}; break;
            case 4:
                byte[] bt=new byte[]{DC3,80,72,4,01,(byte)(bts.length/256),(byte)(bts.length%256)};
                byte[] datas = new byte[7+bts.length];
                int k = 0;
                for(int i=0;i<bt.length;i++){
                    datas[k++]=bt[i];
                }
                for(int i=0;i<bts.length;i++){
                    datas[k++]=bts[i];
                }
                bytes = datas; break;
            case 5: bytes = new byte[]{DC3,80,72,(byte)n,(byte)m}; break;
        }

       return bytes;
    }

    /**
     * 打印光栅位图
     * @param m 0 1 2 3
     * @return
     */
    public static byte[] print_gra_bitmap_set(int m,int xL,int yL,byte[] data){
        byte[] bts = new byte[8+data.length];
        bts[0] = GS;
        bts[1] = 118;
        bts[2] = 48;
//        switch(m){
//            case 0: bts[3] = 48; break;
//            case 1: bts[3] = 49; break;
//            case 2: bts[3] = 50; break;
//            case 3: bts[3] = 51; break;
//        }

        bts[3] = (byte)m;
        bts[4] = (byte)(xL/8%256);
        bts[5] = (byte)(xL/8/256);
        bts[6] = (byte)(yL%256);
        bts[7] = (byte)(yL/256);
        int idx = 8;
        for ( int i = 0; i < data.length; i++ ){
            bts[idx] = data[i];
            idx++;
        }
        //bts[idx] = 0;
        return bts;
    }

    /**
     * 打印机状态
     * n = 1: 传送打印机状态 n = 2: 传送脱机状态 n = 3: 传送错误状态 n = 4: 传送卷纸传感器状态 n = 5：外接传感器查询状态
     * @return
     */
    public static byte[] printer_state(int n){
        return new byte[]{DLE,EOT,(byte)n};
    }

    /**
     *传送打印纸传感器状态
     * @return
     */
    public static byte[] transer_state(){
        return new byte[]{GS,114,1};
    }

    /**
     *查询打印机固件版本
     * @param n
     * @return
     */
    public static byte[] device_version_state(int n){
        return new byte[]{GS,73,(byte)n};
    }

    /**
     *全切
     * @return
     */
    public static byte[] cut_all(){
        return new byte[]{ESC,105};
    }

    /**
     *半切
     * @return
     */
    public static byte[] cut_half(){
        return new byte[]{ESC,109};
    }


}
