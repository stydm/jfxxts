package com.jiufengxinxi.ts.device.equipment.test;

import com.jiufengxinxi.ts.common.enums.DeviceStateEnum;
import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;
import com.jiufengxinxi.ts.device.equipment.rfid.IReadListener;
import com.jiufengxinxi.ts.device.equipment.rfid.RfidOperateOld;

public class RfidTest {

	public static void main(String[] args) throws Exception {
		RfidOperateOld rfidOp =new RfidOperateOld("tmr://COM5",true,new String[] {"1"},2000);
		rfidOp.setReadListener(new RfidListener());
		rfidOp.setDeviceCallback(new DeviceStateCallback());
		rfidOp.setReadType(1);
		rfidOp.init();
		rfidOp.listen();
		Thread.sleep(5000);
		rfidOp.stopListen();
		rfidOp.destroy();
	}
	
}


class RfidListener implements IReadListener{

	@Override
	public void tag(String tid, String epc, int annt, int rssi) {

		System.out.println(String.format("tid:%s, epc:%s, annt:%d, rssi:%d", tid,epc,annt,rssi));
	}
	
}

class DeviceStateCallback implements IDeviceStateCallback{

	@Override
	public <E extends Exception> void crash(Object obj, E exception) {
		System.out.println(exception.getLocalizedMessage());
	}

	@Override
	public void stateChange(DeviceStateEnum state, String desc) {
		System.out.println(state.name()+"    "+state);
		
	}

	@Override
	public void check() {
		// TODO Auto-generated method stub
		
	}
	
}