package com.jiufengxinxi.ts.device.equipment.rfid;

public interface IReadListener {
	
	public void tag(String tid,String epc, int annt, int rssi) ;

}
