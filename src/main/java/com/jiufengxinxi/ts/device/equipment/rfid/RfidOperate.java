package com.jiufengxinxi.ts.device.equipment.rfid;

import com.jiufengxinxi.ts.common.exception.BusinessException;
import com.jiufengxinxi.ts.common.utils.CRCUtil;
import com.jiufengxinxi.ts.common.utils.HexUtil;
import com.jiufengxinxi.ts.device.callback.IDeviceCallback;
import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;
import com.jiufengxinxi.ts.device.interfaces.ICommDevice;
import com.jiufengxinxi.ts.device.interfaces.IReadDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * M6e读写器操作类
 */
//@Component
public class RfidOperate extends ICommDevice implements IReadDevice, IDeviceCallback {

	private int state=0;//0=未初始,1=已经初始，2=监听读卡中,3=读卡中,4=写卡中

	private int readType=0;//0=EPC,1=TID

	private int[] antennas = new int[]{1};

	private int readerPower = 3150;

	/**
	 * 已缓存的卡号
	 */
	private int cacheCardCount = 0;

	/**
	 * 当次刷选卡号时间
	 */
	private int selectTimes = 10;

	private IReadListener readListener;

	private IDeviceStateCallback deviceCallback;

	private RfidOrder rfidOrder = new RfidOrder();
	
	private Logger logger= LoggerFactory.getLogger(this.getClass());

	public RfidOperate(){
		super("/dev/ttyS1", null);
	}

	public RfidOperate(String port, IReadListener readListener) {
		super(port.replace("tmr://",""), null);
		this.readListener = readListener;
	}

	@Override
	public void init(String readerAddress, String[] antennaGroup, int readerPower) {
		readerAddress = readerAddress.replace("tmr://","");
		//this.antennas = antennas;
		this.readerPower = readerPower;

		List<Integer> antennas=new ArrayList<Integer>();
		for(String as:antennaGroup){
			String[] at=as.split(",");
			for(String a:at){
				antennas.add(Integer.parseInt(a));
			}
		}

		this.antennas=new int[antennas.size()];
		for(int i=0;i<this.antennas.length;i++){
			this.antennas[i]=antennas.get(i);
		}

		try {
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 *
	 * @param type 1:初始化;2:打开监听;3:停止监听
	 */
	public synchronized void rfidOperate(int type) throws IOException {
		synchronized (super.getAppName()){
			switch (type){
				case 1:{
					initDevide("M6e");
					if(getSerialPort()==null){
						throw new BusinessException("串口打开失败");
					}
					if(!getVersion().getStatus().equals("0000")){
						throw new BusinessException("初始化读写器失败:获取版本");
					}
					getVersion();

					if(!getPowerModel().getStatus().equals("0000")){
						throw new BusinessException("初始化读写器失败:读取电源模式");
					}
					if(!getProtocol().getStatus().equals("0000")){
						throw new BusinessException("初始化读写器失败:读取协议");
					}

					if(!getMCP().getStatus().equals("0000")){
						throw new BusinessException("初始化读写器失败:获取模块当前频段国家");
					}

					state = 1;
				}break;
				case 2:{
					if(state!=1){
						System.out.println("当前没有初始化，请选初始化");
						return;
					}
					if(!setAntenna(this.antennas).getStatus().equals("0000")){
						throw new BusinessException("准备读卡失败:设置天线");
					}
					if(!setReadPower(this.readerPower).getStatus().equals("0000")){
						throw new BusinessException("准备读卡失败:设置功率");
					}
					if(!saveSetting().getStatus().equals("0000")){
						logger.error("准备读卡失败:保存设置");
					}

					state = 2;

				}break;
				case 3:{
					state = 1;
					//sendOrder(rfidOrder.getClearCache());
					//sendOrder(rfidOrder.getStopRead());
				}break;
			}
		}
	}

	public void startReadTagThread(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true){
					try{
						if(state==2){
							if(!prepare().getStatus().equals("0000")){
								throw new BusinessException("打开读卡失败:读卡准备");
							}
							if(!clearCache().getStatus().equals("0000")){
								throw new BusinessException("打开读卡失败:清除缓存");
							}
							clearCache();
							int tcount = selectRfid(selectTimes);
							if(tcount<0){
								stopListen();
								throw new BusinessException("打开读卡失败:刷选标签");
							}
							if(tcount>0) {
								readTags(tcount,0);
							}
						}else {
							Thread.sleep(20);
						}
					}catch (Exception e){}
				}
			}
		}).start();
	}

	/**
	 * 读卡
	 * @param total 总共要读的标签数
	 * @param ed 已读的标签数
	 * @return
	 */
	public void readTags(int total, int ed) throws IOException {
		if(ed>=total){
			return;
		}
		ReaderData rd = readTidEpc();
		if(!rd.getStatus().equals("0000")){
			throw new BusinessException("读卡失败:读取缓冲区标签");
		}

		for(TagPackage tag:rd.getTags()){

			String cardNum = null;

			if(getReadType()==1) {
				cardNum = tag.getTid();
			}else{
				cardNum = tag.getEpc();
			}

			this.readListener.tag(cardNum, tag.getEpc(), tag.getAntenna(), tag.getRssi());
		}
		ed += rd.getTagCount();
		readTags(total, ed);
	}

	/**
	 * 获取程序指令
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getProgram() throws IOException {
		sendOrder(rfidOrder.getGetProgram());
		System.out.println("获取程序指令");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 获取电源模式令
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getPowerModel() throws IOException {
		sendOrder(RfidOrder.GET_POWER_MODEL);
		System.out.println("获取程序指令");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 获取版本指令
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getVersion() throws IOException {
		sendOrder(RfidOrder.GET_VERSION);
		System.out.println("获取版本指令");
		
		ReaderData rd = rfidOrder.readDataFromByte(readReadData());
		for(int i=0;i<35;i++){
			sendOrder(RfidOrder.EMPTY);
		}
		return rd;
	}

	/**
	 * 重启模块程序
	 * @return
	 * @throws IOException
	 */
	protected void reboot() throws IOException {
		sendOrder(rfidOrder.getRebootReader());
		System.out.println("重启模块程序");
		readReadData();
	}


	/**
	 * 获取模块跳频
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getFrequency() throws IOException {
		sendOrder(rfidOrder.getGetFrequency());
		System.out.println("获取模块跳频");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 获取程序协议
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getProtocol() throws IOException {
		sendOrder(RfidOrder.GET_PROTOCOL);
		System.out.println("获取程序协议");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 设置读卡天线
	 * @return
	 * @throws IOException
	 */
	protected ReaderData setAntenna(int[] ants) throws IOException {
		sendOrder(rfidOrder.getSetAntenna(ants));
		System.out.println("设置读卡天线");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 获取读卡天线
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getAntenna() throws IOException {
		sendOrder(RfidOrder.GET_ANTENNA);
		System.out.println("获取读卡天线");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 设置读卡功率
	 * @return
	 * @throws IOException
	 */
	protected ReaderData setReadPower(int power) throws IOException {
		sendOrder(rfidOrder.getSetReadPower(power));
		System.out.println("设置读卡功率");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 清空读卡缓存
	 * @return
	 * @throws IOException
	 */
	protected ReaderData clearCache() throws IOException {
		sendOrder(RfidOrder.CLEAR_CACHE);
		System.out.println("清空读卡缓存");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 刷选标签
	 * @return
	 * @throws IOException
	 */
	protected int selectRfid(int selectTimes) throws IOException {
		sendOrder(rfidOrder.getSelectRfid(selectTimes));
		System.out.println("刷选标签");
		ReaderData readerData = rfidOrder.readDataFromByte(readReadData());
		if(readerData.getStatus().equals("0000")||readerData.getStatus().equals("0400")) {
			return Integer.valueOf(readerData.getData(),16);
		}
		return -1;
	}

	/**
	 * 读取卡号
	 * @return
	 * @throws IOException
	 */
	protected ReaderData readTidEpc() throws IOException {
		sendOrder(rfidOrder.getReadTidEpc());
		System.out.println("读取卡号");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 获取模块当前频段国家
	 * @return
	 * @throws IOException
	 */
	protected ReaderData getMCP() throws IOException {
		sendOrder(RfidOrder.GET_MODEL_COUNTRY_PREPOINT);
		System.out.println("获取模块当前频段国家");
		return rfidOrder.readDataFromByte(readReadData());
	}

	//prepare
	/**
	 * 读卡前准备
	 * @return
	 * @throws IOException
	 */
	protected ReaderData prepare() throws IOException {
		sendOrder(RfidOrder.PREPARE);
		System.out.println("读卡前准备");
		return rfidOrder.readDataFromByte(readReadData());
	}

	/**
	 * 保存模块设置
	 * @return
	 * @throws IOException
	 */
	protected ReaderData saveSetting() throws IOException {
		sendOrder(rfidOrder.getSaveSetting());
		System.out.println("保存模块设置");
		return rfidOrder.readDataFromByte(readReadData());
	}


	/**
	 * 读取数据，需要设定超时机制
	 * @return
	 * @throws IOException
	 */
	public synchronized byte[] readReadData() throws IOException {
		//readData();
		/*try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		/*byte[] data = null;
		String datastr=null;
		do {
			data = readData(2,5000);
			datastr = HexUtils.bytesToHexString(data).toUpperCase();
		}while(!RfidOrder.genCRC(datastr.substring(0, datastr.length()-4)).equals(datastr));*/
		
		return readData(7,5000);
	}

	@Override
	public void init() throws Exception {
		setDeviceCallback(this);
		super.setBaudrate(115200);
		super.setSynchronizationData(true);
		super.setReciveBytes(true);
		startReadTagThread();
		rfidOperate(1);
	}

	@Override
	public void listen() throws Exception {
		rfidOperate(2);
	}

	@Override
	public void stopListen() throws Exception {
		rfidOperate(3);
	}

	@Override
	public String write(String tag, String targetTag, int antenna) {
		return null;
	}

	@Override
	public List<String> read() throws Exception {
		return null;
	}

	@Override
	public boolean writePower(int readPower) {
		return false;
	}

	@Override
	public int getReadType() {
		return readType;
	}

	@Override
	public void setReadType(int readType) {
		this.readType=readType;
	}

	@Override
	public int getState() {
		return state;
	}

	@Override
	public String getReaderURI() {
		return null;
	}

	@Override
	public int[] getAntennaList() {
		return antennas;
	}

	@Override
	public void setReadListener(IReadListener readListener) {
		this.readListener = readListener;
	}

	@Override
	public String status() {
		return null;
	}

	@Override
	public boolean check() {
		return false;
	}

	@Override
	public void produceSinal(String sinal) {

	}

	@Override
	public void produceSinal(byte[] sinal) {

	}

	@Override
	public void destroy() {
		super.close();
		state=0;
	}

	@Override
	public String getDeviceName() {
		return "M6e";
	}

	@Override
	public void setDeviceCallback(IDeviceStateCallback deviceCallback) {
		this.deviceCallback=deviceCallback;
	}

	private void sendOrder(String order){
		System.out.println("\nsend:"+order);
		write(HexUtil.HexString2Bytes(order));
	}

	public void setState(int state) {
		this.state = state;
	}

	public int[] getAntennas() {
		return antennas;
	}

	public void setAntennas(int[] antennas) {
		this.antennas = antennas;
	}

	public int getReaderPower() {
		return readerPower;
	}

	public void setReaderPower(int readerPower) {
		this.readerPower = readerPower;
	}

	public int getCacheCardCount() {
		return cacheCardCount;
	}

	public void setCacheCardCount(int cacheCardCount) {
		this.cacheCardCount = cacheCardCount;
	}

	public int getSelectTimes() {
		return selectTimes;
	}

	public void setSelectTimes(int selectTimes) {
		this.selectTimes = selectTimes;
	}

	public IReadListener getReadListener() {
		return readListener;
	}

	public RfidOrder getRfidOrder() {
		return rfidOrder;
	}

	public void setRfidOrder(RfidOrder rfidOrder) {
		this.rfidOrder = rfidOrder;
	}

	@Override
	public Object callBack(String gateNo, Object... param) {
		return null;
	}


	/**
	 * RFID 指令及工具集
	 */
	public static class RfidOrder{

		public static final Map<String,int[]> RESULT_INDEX_MAP = new HashMap<String,int[]>();


		public static final String EMPTY = "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff";

		public static final String PREPARE = "ff 02 6c 01 00 4e 88".replace(" ","");
		static{RESULT_INDEX_MAP.put("6C",new int[]{3});}

		/**
		 * 获取程序指令
		 */
		public static final String GET_PROGRAM = "ff000c1d03";
		static{RESULT_INDEX_MAP.put("0C",new int[]{3,5,-2});}


		/**
		 * GEN2 参数获取
		 */
		public static final String GET_GEN2 = "ff 00 03 1d 0c".replace(" ","");
		static{RESULT_INDEX_MAP.put("03",new int[]{3});}



		/**
		 * 频率
		 */
		public static final String GET_FREQUENCY = "ff0197064bbb";
		static{RESULT_INDEX_MAP.put("97",new int[]{3});}

		/**
		 * 获取跳频
		 */
		public static final String FREPOINT = "ff00651d6a";

		/**
		 * 设置协议指令
		 */
		public static final String SET_PROTOCOL = "ff02930005517d";
		static{RESULT_INDEX_MAP.put("93",new int[]{3});}

		/**
		 * 获取协议指令
		 */
		public static final String GET_PROTOCOL = "ff 00 63 1d 6c".replace(" ","");
		static{RESULT_INDEX_MAP.put("63",new int[]{3});}


		/**
		 * 设置天线
		 */
		public static final String SET_ANTENNA = "ff_LEN_9102_DAT__CRC_";
		static{RESULT_INDEX_MAP.put("91",new int[]{3});}

		/**
		 * 获取天线
		 */
		public static final String GET_ANTENNA = "ff016105bdb8";
		static{RESULT_INDEX_MAP.put("61",new int[]{3,7,8});}

		/**
		 * 保存设置
		 */
		public static final String SAVE_SETTING = "ff039d01010137cb";
		static{RESULT_INDEX_MAP.put("9D",new int[]{3});}

		/**
		 * 设置功率
		 */
		public static final String SET_READ_POWER = "ff0292_DAT__CRC_";
		static{RESULT_INDEX_MAP.put("92",new int[]{3});}

		/**
		 * 获取功率
		 */
		public static final String GET_READ_POWER = "ff016201bebc";
		static{RESULT_INDEX_MAP.put("62",new int[]{3});}

		/**
		 * 读TID和EPC
		 */
		public static final String READ_TID_EPC = "ff032901ff001b03";
		static{RESULT_INDEX_MAP.put("29",new int[]{3,8,-2});}


		/**
		 * 停止读卡
		 */
		public static final String STOP_READ = "ff032f0000025e86";
		/**
		 * 停止读卡结果位
		 */
		public static final int[] STOP_READ_RESULT = new int[]{3};
		//static{RESULT_INDEX_MAP.put("2f",STOP_READ_RESULT);}

		/**
		 * 持续读卡
		 */
		public static final String CONTINUE_READ = "ff1c2f00000122000005132210001f03e801ff01092803e8000200000000001b2c";


		/**
		 * 获取GEN指令
		 */
		public static final String GET_GEN = "FF 03 9B 05 00 00 DC E8".replace(" ","");
		static{RESULT_INDEX_MAP.put("9B",new int[]{3});}

		/**
		 * 设置GEN指令
		 */
		public static final String SET_GEN = "ff039b0500_DAT__CRC_";

		/**
		 * 获取模块设置
		 */
		public static final String GET_MODEL_SETTING = "ff016201bebc";

		/**
		 * 获取电源模式
		 */
		public static final String GET_POWER_MODEL = "ff00681d67";
		static{RESULT_INDEX_MAP.put("68",new int[]{3});}

		/**
		 * 获取模块当前频段国家
		 */
		public static final String GET_MODEL_COUNTRY_PREPOINT = "ff 00 67 1d 68".replace(" ","");
		static{RESULT_INDEX_MAP.put("67",new int[]{3});}


		/**
		 * 清除缓存
		 */
		public static final String CLEAR_CACHE = "ff002a1d25";
		static{RESULT_INDEX_MAP.put("2A",new int[]{3});}

		/**
		 * 刷选卡号
		 */
		public static final String SELECT_RFID = "ff1122000017_DAT_01092801f400020000000000_CRC_";
		static{RESULT_INDEX_MAP.put("22",new int[]{3,-6,-2});}


		/**
		 * 重启模块
		 */
		public static final String REBOOT_READER = "ff00041d0b";
		static{RESULT_INDEX_MAP.put("04",new int[]{3});}

		/**
		 * 获取版本
		 */
		public static final String GET_VERSION = "ff 00 03 1d 0c".replace(" ","");
		static{RESULT_INDEX_MAP.put("03",new int[]{3});}






		/**
		 * 获取程序指令
		 */
		public String getGetProgram() {
			return GET_PROGRAM;
		}

		/**
		 * 频率
		 */
		public String getGetFrequency() {
			return GET_FREQUENCY;
		}

		/**
		 * 获取跳频
		 */
		public String getFREPOINT() {
			return FREPOINT;
		}

		/**
		 * 设置协议指令
		 */
		public String getSetProtocol() {
			return SET_PROTOCOL;
		}

		/**
		 * 获取协议指令
		 */
		public String getGetProtocol() {
			return GET_PROTOCOL;
		}

		/**
		 * 设置天线
		 */
		public String getSetAntenna(int[] ants) {
			String antstr = "";
			for(int ant:ants){
				antstr+="0"+ant;
				antstr+="0"+ant;
			}
			String length = 0+String.valueOf(HexUtil.toHexChar(ants.length*2+1));

			return bindData(SET_ANTENNA.replace("_LEN_",length),antstr);
		}

		/**
		 * 获取天线
		 */
		public String getGetAntenna() {
			return GET_ANTENNA;
		}

		/**
		 * 保存设置
		 */
		public String getSaveSetting() {
			return SAVE_SETTING;
		}

		/**
		 * 设置功率
		 */
		public String getSetReadPower(int power) {
			if(power<500 || power>3150){
				throw new BusinessException("读写器功率不在正确范围内(500至3150)："+power);
			}
			String powerStr = Integer.toHexString(power);
//			powerStr = powerStr.length()%2==0?powerStr:("0"+powerStr);
//			powerStr = powerStr.length()<=2?"00"+powerStr:powerStr;
			
			for(int i=4-powerStr.length();i>0;i--) {
				powerStr="0"+powerStr;
			}
			//powerStr+=powerStr;

			return bindData(SET_READ_POWER, powerStr);
		}

		/**
		 * 获取功率
		 */
		public String getGetReadPower() {
			return GET_READ_POWER;
		}

		/**
		 * 读TID和EPC
		 */
		public String getReadTidEpc() {
			return READ_TID_EPC;
		}

		/**
		 * 持续读卡
		 */
		public String getContinueRead() {
			return CONTINUE_READ;
		}

		/**
		 * 获取GEN指令
		 */
		public String getGetGen() {
			return GET_GEN;
		}

		/**
		 * 设置GEN指令
		 */
		public String getSetGen() {
			return SET_GEN;
		}

		/**
		 * 获取模块设置
		 */
		public String getGetModelSetting() {
			return GET_MODEL_SETTING;
		}

		/**
		 * 获取电源模式
		 */
		public String getGetPowerModel() {
			return GET_POWER_MODEL;
		}

		/**
		 * 清除缓存
		 */
		public String getClearCache() {
			return CLEAR_CACHE;
		}

		/**
		 * 刷选卡号
		 */
		public String getSelectRfid(int readTime) {
			String readTimeStr = Integer.toHexString(readTime);
			readTimeStr = readTimeStr.length()%2==0?readTimeStr:("0"+readTimeStr);
			readTimeStr = readTimeStr.length()<=2?"00"+readTimeStr:readTimeStr;
			return bindData(SELECT_RFID, readTimeStr);
		}

		/**
		 * 停止读卡
		 * @return
		 */
		public String getStopRead() {
			return STOP_READ;
		}

		/**
		 * 绑定数据
		 * @param order
		 * @param data
		 * @return
		 */
		public static String bindData(String order, String data){
			order = order.replace("_DAT_",data);
			return genCRC(order);
		}


		/**
		 * 生成
		 * @param order
		 * @return
		 */
		public static String genCRC(String order){
			order = order.replace("_CRC_","").substring(2);
			return ("FF"+order+ CRCUtil.getCRC4(HexUtil.HexString2Bytes(order))).toUpperCase();
		}

		/**
		 * 重启模块
		 */
		public static String getRebootReader() {
			return REBOOT_READER;
		}

//public Map<>


		public ReaderData readDataFromByte(byte[] data){
			ReaderData readerData = new ReaderData();
			try {
				String datastr = HexUtil.bytesToHexString(data).toUpperCase();
				System.out.println("recive : "+datastr);
				if(datastr.length()<14) {
					readerData.setStatus("FFFF1");
					return readerData;
				}
				String type = datastr.substring(4, 6);
				int status_start = 0;
				int status_end = 0;
				int data_start = 0;
				int data_end = 0;

				int[] indexs = RESULT_INDEX_MAP.get(type);
				/*if (type.equals("0C")) { //获取程序指令
					indexs = GET_PROGRAM_RESULT;
				} else if (type.equals("29")) {
					indexs = READ_TID_EPC_RESULT;
				} else if (type.equals("91")) {
					indexs = SET_ANTENNA_RESULT;
				} else if (type.equals("9D")) {
					indexs = SAVE_SETTING_RESULT;
				} else if (type.equals("92")) {
					indexs = SET_READ_POWER_RESULT;
				} else if (type.equals("62")) {
					indexs = GET_READ_POWER_RESULT;
				} else if (type.equals("2A")) {
					indexs = CLEAR_CACHE_RESULT;
				} else if (type.equals("22")) {
					indexs = SELECT_RFID_RESULT;
				} else if (type.equals("97")) {
					indexs = GET_FREQUENCY_RESULT;
				} else if (type.equals("93")) {
					indexs = SET_PROTOCOL_RESULT;
				} else if (type.equals("04")) {
					indexs = REBOOT_READER_RESULT;
				}*/

				if (indexs != null) {

					readerData.setType(type);
					status_start = (indexs[0]) * 2;
					status_end = status_start + 4;

					readerData.setStatus(datastr.substring(status_start, status_end));
					if (indexs.length > 1) {//有返回数据
						if (indexs[1] < 0){
							data_start = datastr.length() + (indexs[1] * 2);
						}else{
							data_start = (indexs[1]) * 2;
						}

						if (indexs[2] < 0) {
							data_end = datastr.length() + (indexs[2] * 2);
						} else {
							data_end = (indexs[2]) * 2;
						}
						readerData.setData(datastr.substring(data_start, data_end));
					}

					//return readerData;
				} else {
					System.out.println("暂无该业务类型解析");
				}
			}catch (Exception e){
				readerData.setStatus("FFFF");
			}

			return readerData;
		}

	}

	public static void main(String[] args){
		String data = "FF07220000000013000000018B58";
		/*RfidOrder rdifOrder = new RfidOrder();
		ReaderData rdata = rdifOrder.readDataFromByte(HexUtils.HexString2Bytes(data));
		System.out.println(rdata.getType());
		System.out.println(rdata.getStatus());
		System.out.println(rdata.getData());*/
		/*String t = "FF0168000000A4BF01680000";
		t=t.replace(" ", "").toLowerCase();
		System.out.println(t.length());
		System.out.println(RfidOrder.genCRC(t));*/
		System.out.println(Integer.valueOf("14", 16));
		//rdata.getTags();
	}
}


class ReaderData{
	private String type;
	private String status;
	private String data;
	private int tagCount;
	private List<TagPackage> tags;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public List<TagPackage> getTags() {
		if(data!=null) {
			tagCount = Integer.valueOf(data.substring(0, 2),16);
			if (tags == null && type.equals("29")) {
				tags = new ArrayList<TagPackage>();
				String tagDataStr = data.substring(2);
				do{
					try {
						int tidlstart = 26;//tid 长度位置开始
						int tidlend = 30;//tid 长度位置结束

						int tidlen = Integer.valueOf(tagDataStr.substring(26, 30), 16);//数据区长度
						int epclstart = 30 + tidlen + 2;//epc 长度位置开始
						int epclend = epclstart + 4;//epc 长度位置结束
						int epclen = Integer.valueOf(tagDataStr.substring(epclstart, epclend), 16) / 8 * 2; //epc长度为bit,1byte=8bit 所以要除以8,
						// 以16进制的1个字节表示为2个字符，所以还要乘以2

						TagPackage tag = new TagPackage();
						tag.setTid(tagDataStr.substring(tidlend, tidlen + tidlend));
						tag.setEpc(tagDataStr.substring(epclend, epclen + epclend));
						tag.setEpc(tag.getEpc().substring(4,tag.getEpc().length()-4));
						tag.setReadCout(Integer.valueOf(tagDataStr.substring(0, 2), 16));
						tag.setRssi(128-Integer.valueOf(tagDataStr.substring(2, 4), 16));
						tag.setAntenna(Integer.valueOf(tagDataStr.substring(4, 6), 16));

						tags.add(tag);

						int culen = epclend + epclen;
						data = data.substring(culen);
					}catch (Exception e){
						return tags;
					}
					//tagDataStr = data.substring()
				}while (data.length()>4);
			}
		}
		return tags;
	}

	public void setTags(List<TagPackage> tags) {
		this.tags = tags;
	}

	public int getTagCount() {
		return tagCount;
	}

	public void setTagCount(int tagCount) {
		this.tagCount = tagCount;
	}
}

class TagPackage {
	private int readCout;
	private int rssi;
	private int antenna;
	private String data;
	private String epc;
	private String tid;

	public int getReadCout() {
		return readCout;
	}

	public void setReadCout(int readCout) {
		this.readCout = readCout;
	}

	public int getRssi() {
		return rssi;
	}

	public void setRssi(int rssi) {
		this.rssi = rssi;
	}

	public int getAntenna() {
		return antenna;
	}

	public void setAntenna(int antenna) {
		this.antenna = antenna;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getEpc() {
		return epc;
	}

	public void setEpc(String epc) {
		this.epc = epc;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}
}
