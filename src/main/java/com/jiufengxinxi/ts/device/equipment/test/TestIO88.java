package com.jiufengxinxi.ts.device.equipment.test;


import com.jiufengxinxi.ts.common.utils.HexUtil;
import com.jiufengxinxi.ts.common.utils.socket.client.MultiThreadClient;

public class TestIO88 {

	public static void main(String[] args) throws InterruptedException {
		MultiThreadClient mtc = new MultiThreadClient("10.197.32.122",8899);
		mtc.setSocketRequestHandle(new ClinetSocketRequestHandle());
		mtc.setCharFilter(new MultiThreadClient.HexFilter());
		mtc.connection();
		mtc.send(HexUtil.toBytes("61646d696e0d0a"));
		mtc.send(HexUtil.toBytes("55aa000300010105"));
		
		for(int i=0;i<10;i++) {
			mtc.send(HexUtil.toBytes("55aa000300010305"));
			Thread.sleep(500);
			mtc.send(HexUtil.toBytes("55aa000300020306"));
			Thread.sleep(500);
		}
		
		while(true) {
			mtc.send(HexUtil.toBytes("55aa0002001416"));
			
			
			Thread.sleep(50);
		}
	}
	
}