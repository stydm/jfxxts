package com.jiufengxinxi.ts.device.equipment.rfid;

import com.thingmagic.Reader;
import com.thingmagic.SerialReader.StatusReport;
import com.thingmagic.StatusListener;

public class ReaderStatusListener implements StatusListener {

	@Override
	public void statusMessage(Reader arg0, StatusReport[] arg1) {
		for(StatusReport status:arg1){
			System.out.println("当前状态："+status.toString());
		}
		
	}

}
