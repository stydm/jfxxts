package com.jiufengxinxi.ts.device.equipment.test;

import com.jiufengxinxi.ts.device.equipment.IO88Device;

public class TestIO88Device {

	public static void main(String[] args) {
		IO88Device device = new IO88Device("10.197.32.122",8899,"admin\r\n");
		device.setHandler(new ClinetSocketRequestHandle());
		device.startup();
		device.readInIOSinalSend();
	}
	
}
