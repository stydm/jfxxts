package com.jiufengxinxi.ts.device.equipment.test;

import com.jiufengxinxi.ts.device.callback.IODeviceCallback;
import com.jiufengxinxi.ts.device.interfaces.IOCommDevice;

public class IoCommonTest {
	
	public static int flag = 0 ;
	
	public static void main(String[] args) {
		String addr = "01";
		
		IODeviceCallback iocalback = new IOCallback(addr);
		
		IOCommDevice iocomm = new IOCommDevice("COM4",addr,iocalback);
		
		
		iocomm.startup();
		iocomm.startIOStream();
		
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					try {
						
						if(flag==1) {
							iocomm.writeCommand("0", "01");
						}else {
							iocomm.writeCommand("0", "00");
						}
						
						Thread.sleep(1000);
						
					}catch(Exception e) {}
				}
			}
		}).start();
		
	}
	

}

class IOCallback extends IODeviceCallback{

	public IOCallback(String channel) {
		super(channel);
	}

	@Override
	public void trigger(String gateNo, String trigger) {
		
		System.out.println(trigger.substring(7));
		System.out.println(trigger.substring(6));
		if(trigger.substring(7).equals("1")) {
			IoCommonTest.flag = 1;
		}else {
			IoCommonTest.flag = 0;
		}
		
		System.out.println(trigger);
	}
	
}