package com.jiufengxinxi.ts.device.equipment.printer;


import com.jiufengxinxi.ts.common.utils.HexUtil;

public class T500PrinterAdapter {

    public static final int ESC = 27;// 换码
    public static final int FS = 28;// 文本分隔符
    public static final int GS = 29;// 组分隔符
    public static final int DLE = 16;// 数据连接换码
    public static final int EOT = 4;// 传输结束
    public static final int ENQ = 5;// 询问字符
    public static final int SP = 32;// 空格
    public static final int HT = 9;// 横向列表
    public static final int LF = 10;// 打印并换行（水平定位）
    public static final int CR = 13;// 归位键
    public static final int FF = 12;// 走纸控制（打印并回到标准模式（在页模式下） ）
    public static final int CAN = 24;// 作废（页模式下取消打印数据 ）
    public static final int DC3 = 19;// 作废（页模式下取消打印数据 ）



    /**
     * 水平定位
     * 移动打印位置到下一个水平定位点的位置。
     * @return
     */
    public static byte[] hor_pos(){
        return new byte[]{HT};
    }

    /**
     * 打印并换行
     * 把打印缓冲区中的数据打印出来，并且按照当前行间距，把打印纸向前推进一行。
     * @return
     */
    public static byte[] print_line_feed(){
        return new byte[]{LF};
    }

    /**
     * 页模式下取消打印数据
     * 在页模式下，删除当前打印区域中所有打印数据。
     * @return
     */
    public static byte[] cancel_print(){
        return new byte[]{CAN};
    }

    /**
     * 实时状态传送
     * 实时地传送打印机状态。参数 n 用来指定所要传送的打印机状态。定义如下: n = 1: 传送打印机状态 n = 2: 传送脱机状态 n = 3: 传送错误状态 n = 4: 传送卷纸传感器状态
     * @return
     */
    public static byte[] real_time(byte t){
        return new byte[]{DLE,EOT,t};
    }


    /**
     * 页模式下取消打印数据
     * 在页模式下，删除当前打印区域中所有打印数据。
     * @return
     */
    public static byte[] page_print(){
        return new byte[]{ESC,FF};
    }


    /**
     *
     * @return
     */
    public static byte[] goto_start(){
        return new byte[]{FF};
    }


    /**
     * 设置字符右间距
     * 设置字符右间距为[n 0.125 毫米]。
     * @return
     */
    public static byte[] character_pitch_right(int t){
        return new byte[]{ESC,SP,(byte)t};
    }

    /**
     * 设置绝对打印位置
     * 设定从一行的开始到将要打印字符的位置之间的距离。
     * @return
     */
    public static byte[] ab_print_site(byte nL,byte nH){
        return new byte[]{ESC,36,nL,nH};
    }

    /**
     * 设定/解除下划线
     * 基于以下的n值，设定/解除下划线模式
     * @return
     */
    public static byte[] under_line(byte t){
        return new byte[]{ESC,45,t};
    }

    /**
     * 设置行间距
     * 设置行间距为 [n 0.125 毫米].
     * @return
     */
    public static byte[] row_spacing(int t){
        return new byte[]{ESC,51,(byte)t};
    }

    /**
     * 初始化打印机
     * 清除打印缓冲区中的数据，复位打印机模式到电源打开时打印机的有效模式。
     * @return
     */
    public static byte[] init_print(){
        return new byte[]{ESC,64};
    }

    /**
     * 设定/解除加重打印
     * 设定或解除加重打印模式。
     * @return
     */
    public static byte[] print_weight(byte t){
        return new byte[]{ESC,69,t};
    }


    /**
     * 打印并进纸
     * 打印输出打印缓冲区中的数据，并进纸 [n 0.125 毫米].
     * @return
     */
    public static byte[] print_inp(byte t){
        return new byte[]{ESC,74,t};
    }

    /**
     * 选择页模式
     * 从标准模式切换到页模式。
     * @return
     */
    public static byte[] page_model(){
        return new byte[]{ESC,76};
    }

    /**
     * 选择标准模式
     * 从页模式切换到标准模式。
     * @return
     */
    public static byte[] stand_model(){
        return new byte[]{ESC,83};
    }


    /**
     * 选择字型
     * 选择字符字型
     * @return
     */
    public static byte[] select_font(byte t){
        return new byte[]{ESC,77,t};
    }

    /**
     * 选择国际字符集
     * 按照下表选择n的值，设置国际字符集。
     * @return
     */
    public static byte[] intl_char_set(byte t){
        return new byte[]{ESC,82,t};
    }

    /**
     * 在页模式下选择打印方向
     * 在页模式下选择打印方向和起始位置。
     * @return
     */
    public static byte[] direc_print(byte t){
        return new byte[]{ESC,84,t};
    }


    /**
     * 选择对齐方式
     * 将一行数据按照指定的位置对齐
     * 0 左
     * 1 中
     * 2 右
     * @return
     */
    public static byte[] align_print(int t){
        return new byte[]{ESC,97,(byte)t};
    }

    /**
     * 选择打印纸传感器以输出缺纸信号
     * @return
     */
    public static byte[] set_pe_sinal(byte t){
        return new byte[]{ESC,99,51,t};
    }

    /**
     * 打印并进纸 n 行
     * 打印输出打印缓冲区中的数据，并进纸 n 行。
     * @return
     */
    public static byte[] print_inp_n(byte t){
        return new byte[]{ESC,100,t};
    }

    /**
     * 设置/解除颠倒打印模式
     * 设置或解除颠倒打印模式。
     * @return
     */
    public static byte[] reversal_print_model(byte t){
        return new byte[]{ESC,123,t};
    }


    /**
     * 选择字符大小
     * @param n
     * @return
     */
    public static byte[] characterSizeSet(int n){
        byte realSize = 0;
        switch (n) {
            case 1:
                realSize = 0;
                break;
            case 2:
                realSize = 17;
                break;
            case 3:
                realSize = 42;
                break;
            case 4:
                realSize = 51;
                break;
            case 5:
                realSize = 68;
                break;
            case 6:
                realSize = 85;
                break;
            case 7:
                realSize = 102;
                break;
            case 8:
                realSize = 119;
                break;
        }
        return new byte[]{FS,33,realSize};
    }

    /**
     * 设定字符大小
     * @param n
     * @return
     */
    public static byte[] font_size(int n){
        return new byte[]{GS,33,(byte)n};
    }


    /**
     * 设置左边距
     * @param width
     * @return
     */
    public static byte[] margin_left_set(int width){
        return new byte[]{GS,76,(byte)(width%256),(byte)(width/256)};
    }

    /**
     * 设置打印区域宽度
     * @param nL
     * @param nH
     * @return
     */
    public static byte[] print_area_set(int nL,int nH){
        return new byte[]{GS,87,(byte)nL,(byte)nH};
    }

    /**
     *传送打印纸传感器状态
     * @return
     */
    public static byte[] transer_state(){
        return new byte[]{GS,114,1};
    }

    /**
     * 打印光栅位图
     * @param m 0 1 2 3
     * @return
     */
    public static byte[] print_gra_bitmap_set(int m,int xL,int yL,byte[] data){
        byte[] bts = new byte[8+data.length];
        bts[0] = GS;
        bts[1] = 118;
        bts[2] = 48;
//        switch(m){
//            case 0: bts[3] = 48; break;
//            case 1: bts[3] = 49; break;
//            case 2: bts[3] = 50; break;
//            case 3: bts[3] = 51; break;
//        }

        bts[3] = (byte)m;
        bts[4] = (byte)(xL/8%256);
        bts[5] = (byte)(xL/8/256);
        bts[6] = (byte)(yL%256);
        bts[7] = (byte)(yL/256);
        int idx = 8;
        for ( int i = 0; i < data.length; i++ ){
            bts[idx] = data[i];
            idx++;
        }
        //bts[idx] = 0;
        return bts;
    }

    /**
     * 设定汉字打印模式
     * @return
     */
    public static byte[] chinese_model_set(){
        return new byte[]{FS,38};
    }

    /**
     * 取消汉字打印模式
     * @return
     */
    public static byte[] chinese_ascii_set(){
        return new byte[]{FS,46};
    }


    /**
     * 设定条码大小
     * @return
     */
    public static byte[] qrcode_size(int n){
        return new byte[]{GS,1,3,(byte)n};
    }

    /**
     * 设定条码纠错等级
     * @return
     */
    public static byte[] qrcode_w_level(int n){
        return new byte[]{GS,1,4,(byte)n};
    }


    /**
     * 设定条码数据。
     * @return
     */
    public static byte[] qrcode_set(String content){
        byte[] bts = content==null?null:content.getBytes();

        int nh = bts.length/256;
        int nl = bts.length%256;
        byte[] paras = new byte[]{GS,1,1,(byte)nl,(byte)nh};
        byte[] datas = new byte[paras.length+bts.length];

        int k = 0;
        for(int i=0;i<paras.length;i++){
            datas[k++]=paras[i];
        }
        for(int i=0;i<bts.length;i++){
            datas[k++]=bts[i];
        }
        return datas;
    }

    /**
     * 打印条码
     * @return
     */
    public static byte[] qrcode_print(){
        return new byte[]{GS,1,2};
    }


    /**
     * 选择字型
     * @return
     */
    public static byte[] font_type(int n){
        return new byte[]{ESC,77,(byte)n};
    }

    /**
     * 全切纸
     * @return
     */
    public static byte[] cut_all(){
        return new byte[]{GS,86,66,(byte)255};
    }

    /**
     * 半切纸
     * @return
     */
    public static byte[] cut_half(){
        return new byte[]{GS,86,1};
    }


    /**
     * 选择切纸
     * @return
     */
    public static byte[] cut_diy(int n){
        return new byte[]{GS,86,66,(byte)n};
    }



    /**
     * 选择国际字符集
     * n	字符集
     * 0	美国
     * 1	法国
     * 2	德国
     * 3	英国
     * 4	丹麦 I
     * 5	瑞典
     * 6	意大利
     * 7	西班牙 I
     * 8	日本
     * 9	挪威
     * 10	丹麦 II
     * @return
     */
    public static byte[] intl_char_set(int n){
        return new byte[]{ESC,82,(byte)n};
    }


    /**
     *编号	字符页	编号	字符页
     * 0	PC437[美国欧洲标准]	14	PC1251
     * 1	PC737	15	PC1252
     * 2	PC775	16	PC1253
     * 3	PC850	17	PC1254
     * 4	PC852	18	PC1255
     * 5	PC855	19	PC1256
     * 6	PC857	20	PC1257
     * 7	PC858	21	PC928
     * 8	PC860	22	Hebrew old
     * 9	PC862	23	IINTEL CHAR
     * 10	PC863	24	Katakana
     * 11	PC864	25	特殊符号 00-1F
     * 12	PC865	26	SPACE PAGE
     * 13	PC866
     * @param n
     * @return
     */
    public static byte[] page_char_set(int n){
        return new byte[]{ESC,116,(byte)n};
    }

    /**
     * 选择西文字符打印模式
     * @param size  0 24*24  ;  1 16*16
     * @param strong  1 加粗
     * @param dbw   1 双倍宽
     * @param dbh   1 双倍高
     * @param underline     1 下划线
     * @return
     */
    public static byte[] characterWestSet(int size,int strong,int dbw,int dbh,int underline){
        String s = underline+"0"+dbw+dbh+strong+"00"+size;
        return new byte[]{ESC,33,(byte) HexUtil.binaryToDecimal(s)};
    }

    /**
     * 选择西文字符打印模式
     * @param d  0 24*24  ;  1 16*16
     * @return
     */
    public static byte[] characterWestSet(String d){
        return new byte[]{ESC,33,(byte) HexUtil.binaryToDecimal(d)};
    }

    /**
     * 选择汉字符打印模式
     * @param size  0 24*24  ;  1 16*16
     * @param dbw   1 双倍宽
     * @param dbh   1 双倍高
     * @param underline     1 下划线
     * @return
     */
    public static byte[] characterChineseSet(int size,int dbw,int dbh,int underline){
        String s = underline+"00"+dbh+dbw+"0"+size;
        return new byte[]{FS,33,(byte) HexUtil.binaryToDecimal(s)};
    }

    /**
     * 选择汉字符打印模式
     * @return
     */
    public static byte[] characterChineseSet(String d){
        return new byte[]{FS,33,(byte) HexUtil.binaryToDecimal(d)};
    }

    /**
     * 打印机状态
     * n = 1: 传送打印机状态 n = 2: 传送脱机状态 n = 3: 传送错误状态 n = 4: 传送卷纸传感器状态 n = 5：外接传感器查询状态
     * @return
     */
    public static byte[] printer_state(int n){
        return new byte[]{DLE,EOT,(byte)n};
    }


    public static byte[] print_paper_out(int n){
        return new byte[]{ESC,75,(byte)n};
    }


    public static byte[] print_width(int nL, int nH){
        return new byte[]{GS,87,(byte)nL,(byte)nH};
    }
}
