package com.jiufengxinxi.ts.device.equipment.rfid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Map;

public class ReaderStopListen implements IReaderStopListen {
	
	//private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	
	private String businessUrl;

	private Logger logger= LoggerFactory.getLogger(this.getClass());
	
	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	
	@Override
	public void stop(Map workMap) {
	}

	public String getBusinessUrl() {
		return businessUrl;
	}

	public void setBusinessUrl(String businessUrl) {
		this.businessUrl = businessUrl;
	}

}
