package com.jiufengxinxi.ts.device.equipment;


import com.jiufengxinxi.ts.device.callback.IDeviceCallback;
import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;
import com.jiufengxinxi.ts.device.interfaces.ICommDevice;

public class CommDevice extends ICommDevice {

	public CommDevice(String port, IDeviceCallback deviceCallback) {
		super(port, deviceCallback);
	}

	public CommDevice(String port,int baudrate,IDeviceCallback deviceCallback){
		super(port, baudrate, deviceCallback);
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public String getDeviceName() {
		return "comm";
	}

	@Override
	public void produceSinal(String sinal) {
		System.out.println("串口信号:"+sinal);
		getDeviceCallback().callBack(getPort(), sinal);
	}

	@Override
	public void produceSinal(byte[] sinal) {

	}

	@Override
	public void setDeviceCallback(IDeviceStateCallback deviceCallback) {
	}

}
