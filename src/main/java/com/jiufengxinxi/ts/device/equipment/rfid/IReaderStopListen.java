package com.jiufengxinxi.ts.device.equipment.rfid;

import java.util.Map;

public interface IReaderStopListen {

	public void stop(Map workMap);
	
}
