package com.jiufengxinxi.ts.device.callback.impl;


import com.jiufengxinxi.ts.common.utils.socket.ISocketRequestHandle;
import com.jiufengxinxi.ts.device.callback.IODeviceCallback;
import com.jiufengxinxi.ts.device.interfaces.IOCommDevice;

public class ITriggerCallbackImpl extends IODeviceCallback {
	
	private ISocketRequestHandle socketRequestHandle;

	public ITriggerCallbackImpl(String channel,ISocketRequestHandle socketRequestHandle) {
		super(channel);
		this.socketRequestHandle=socketRequestHandle;
	}

	@Override
	public void trigger(String gateNo, String trigger) {
		// TODO Auto-generated method stub
		//System.out.println(trigger);
		
		/**
		 * 转发到通用处理器
		 */
		if(socketRequestHandle!=null) {
			socketRequestHandle.request(trigger, gateNo);
		}
	}

	
	public static void main(String[] args) {
		IODeviceCallback ioCallback=new ITriggerCallbackImpl("01",null);
		IOCommDevice iocomm=new IOCommDevice("/dev/ttyUSB0","01",ioCallback);
		iocomm.setProIoMessage(false);
		iocomm.startup();
	}
}
