package com.jiufengxinxi.ts.device.callback;

public abstract class IODeviceCallback implements IDeviceCallback{
	
	private String channel;
	
	private int[] chans;
	
	private String state="";
	
	public IODeviceCallback(String channel) {
		super();
		this.channel = channel;
		String[] temps=channel.split(",");
		chans=new int[temps.length];
		for(int i=0;i<temps.length;i++){
			chans[i]=Integer.parseInt(temps[i]);
		}
	}


	@Override
	public Object callBack(String gateNo,Object... param) {
		//System.out.println(param[0]);
		if(gateNo==null) {
			gateNo="";
		}
		//synchronized(gateNo){
			/*System.out.println("ori_ "+param[0]);
			String sinal=new StringBuffer(param[0].toString()).reverse().toString();
			
			//logger.info(gateNo+"道设备回调===================================IO原始信号："+sinal);
			
			char[] sinals=sinal.toCharArray();
			String tempStr="";
			for(int i=0;i<chans.length;i++){
				tempStr+=","+i+"="+sinals[chans[i]];
			}
			tempStr=tempStr.substring(1);
			try{
				if(!tempStr.equals(state)){
					//logger.info(gateNo+"道设备回调===================================IO信号："+tempStr);
					trigger(gateNo,tempStr);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			state=tempStr;*/
//			System.out.println(gateNo);
//			System.out.println(param[0].toString());
			trigger(gateNo,param[0].toString());
			return null;
		//}
	}
	
	public abstract void trigger(String gateNo,String trigger);

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}


	public int[] getChans() {
		return chans;
	}


	public void setChans(int[] chans) {
		this.chans = chans;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}

}
