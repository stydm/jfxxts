package com.jiufengxinxi.ts.device.callback;

import com.jiufengxinxi.ts.common.enums.DeviceStateEnum;

public interface IDeviceStateCallback {

	public <E extends Exception> void crash(Object obj, E exception);

	public void stateChange(DeviceStateEnum state, String desc);

	public void check();
}
