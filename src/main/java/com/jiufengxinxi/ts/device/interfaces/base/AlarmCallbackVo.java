package com.jiufengxinxi.ts.device.interfaces.base;

import java.util.Date;

public class AlarmCallbackVo {

    private AlarmCallbackEnum alarmCallbackEnum;
    private String result;
    private int num;
    private String[] imagePaths;
    private String from;
    private String fromIp;
    private Date resultTime=null;

    public AlarmCallbackEnum getAlarmCallbackEnum() {
        return alarmCallbackEnum;
    }

    public void setAlarmCallbackEnum(AlarmCallbackEnum alarmCallbackEnum) {
        this.alarmCallbackEnum = alarmCallbackEnum;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String[] getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(String[] imagePaths) {
        this.imagePaths = imagePaths;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromIp() {
        return fromIp;
    }

    public void setFromIp(String fromIp) {
        this.fromIp = fromIp;
    }

    public Date getResultTime() {
        return resultTime;
    }

    public void setResultTime(Date resultTime) {
        this.resultTime = resultTime;
    }
}
