package com.jiufengxinxi.ts.device.interfaces;

import com.jiufengxinxi.ts.device.callback.IDeviceCallback;

/**
 * 设备基础接口
 * @author roger
 *
 */
public abstract class IDviceGeneral implements IDeviceCheckGeneral {
	
	/**
	 * 闸口号
	 */
	private String gateNo;
	
	private boolean enableCheck=true;
	
	private int reConnCount=0;

	/**
	 * 回调方法
	 */
	private IDeviceCallback deviceCallback;
	
	/*public IDviceGeneral(String gateNo){
		this.gateNo=gateNo;
	}*/
	
	
	/**
	 * 初始化
	 * @param gateNo
	 * @param param
	 */
	public void init(String gateNo,String ... param){
		setGateNo(gateNo);
		initDevide(param);
	};
	
	/**
	 * 初始化设备
	 * @param param
	 */
	public abstract void initDevide(String ... param);
	

	public String getGateNo() {
		if(gateNo==null) {
			return "";
		}
		return gateNo;
	}

	public void setGateNo(String gateNo) {
		this.gateNo = gateNo;
	}

	public IDeviceCallback getDeviceCallback() {
		return deviceCallback;
	}

	public void setDeviceCallback(IDeviceCallback deviceCallback) {
		this.deviceCallback = deviceCallback;
	}

	public boolean isEnableCheck() {
		return enableCheck;
	}

	public void setEnableCheck(boolean enableCheck) {
		this.enableCheck = enableCheck;
	}
	
	public int getReConnCount(){
		return reConnCount;
	}

	public void setReConnCount(int reConnCount) {
		this.reConnCount = reConnCount;
	}
	
}
