package com.jiufengxinxi.ts.device.interfaces;

import com.jiufengxinxi.ts.device.callback.IDeviceStateCallback;

public interface IDeviceCheckGeneral {

	/**
	 * 设备状态
	 * @return
	 */
	public String status();
	
	/**
	 * 检查设备
	 * @return
	 */
	public boolean check();
	
	/**
	 * 
	 */
	public void destroy();
	
	/**
	 * 
	 */
	public void startup();
	
	
	public String getDeviceName();
	
	public void setDeviceCallback(IDeviceStateCallback deviceCallback);
	
}
