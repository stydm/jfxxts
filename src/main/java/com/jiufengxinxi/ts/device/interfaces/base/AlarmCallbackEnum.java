package com.jiufengxinxi.ts.device.interfaces.base;

public enum AlarmCallbackEnum {

    SNAPSHOT("0"," 抓拍"),
    PLATE_DETE("1"," 车牌识别"),
    ALARM("2","报警"),
    VEHICLE_DETE("3","车辆检测"),
    FACE_DETE("4","人脸检测");


    private String value;

    private String desc;

    AlarmCallbackEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
