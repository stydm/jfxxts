package com.jiufengxinxi.ts.device.interfaces;

import com.jiufengxinxi.ts.device.equipment.rfid.IReadListener;
import com.thingmagic.ReaderException;

import java.util.List;


public interface IReadDevice extends IDeviceCheckGeneral {

	public void init() throws Exception;
	
	public void listen() throws Exception;
	
	public void stopListen() throws Exception;
	
	public String write(String tag,String targetTag,int antenna);
	
	public List<String> read() throws Exception;
	
	public boolean writePower(int readPower);
	
	public int getReadType();

	public void setReadType(int readType);

	public void init(String readerAddress, String[] antennas, int readerPower) throws ReaderException;
	
	public int getState();
	
	
	public String getReaderURI();


	public int[] getAntennaList();

	public void setReadListener(IReadListener readListener);
	
}
