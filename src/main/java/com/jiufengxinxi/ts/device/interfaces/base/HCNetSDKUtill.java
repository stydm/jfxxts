package com.jiufengxinxi.ts.device.interfaces.base;

public class HCNetSDKUtill {

    public static HCNetSDK hCNetSDK =null;

    private static boolean INITED = false;

    public synchronized static void INIT_SDK(){
        if(INITED){
            return;
        }

        hCNetSDK = HCNetSDK.INSTANCE;

        hCNetSDK.NET_DVR_Cleanup();

        hCNetSDK.NET_DVR_Init();
        hCNetSDK.NET_DVR_SetConnectTime(2000,1);
        hCNetSDK.NET_DVR_SetReconnect(10000, true);

        INITED = true;
    }

    public synchronized static void DESTORY(){
        hCNetSDK.NET_DVR_Cleanup();
        INITED = false;
    }

    public synchronized static void NET_DVR_SetDVRMessageCallBack_V30(HCNetSDK.FMSGCallBack callback){
        hCNetSDK.NET_DVR_SetDVRMessageCallBack_V30(callback,null);
    }
}
