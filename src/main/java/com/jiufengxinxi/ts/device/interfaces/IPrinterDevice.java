package com.jiufengxinxi.ts.device.interfaces;

public interface IPrinterDevice {

    int SetDevname(int iDevtype, String cDevname, int iBaudrate);
    int SetUsbportauto();
    int SetInit();
    int PrintSelfcheck();
    int PrintString(String strData,int iImme);
    int PrintFeedline(int iLine);
    int GetStatus();
    int GetStatusspecial();
    int PrintCutpaper(int iMode);
    int SetClean();
    int SetReadZKmode(int mode);
    int SetCodepage(int country,int CPnumber);
    int PrintChargeRow();
    int PrintFeedDot(int Lnumber);
    int SetLinespace(int iLinespace);
    int SetSpacechar(int iSpace);
    int SetLeftmargin(int iLeftspace);
    int SetSizechar(int iHeight,int iWidth,int iUnderline,int iAsciitype);
    int SetSizetext(int iHeight,int iWidth);
    int SetAlignment(int iAlignment);
    int SetBold(int iBold);
    int SetRotate(int iRotate);
    int SetDirection(int iDirection);
    int SetWhitemodel(int iWhite);
    int SetItalic(int iItalic);
    int SetUnderline(int underline);
    int SetSizechinese(int iHeight,int iWidth,int iUnderline,int iChinesetype);
    int SetSpacechinese(int iChsleftspace,int iChsrightspace);
    int SetHTseat(String bHTseat,int iLength);
    int PrintNextHT();

    int PrintQrcode(String strData,int iLmargin,int iMside,int iRound);
    int PrintRemainQR();
    int Print1Dbar(int iWidth,int iHeight,int iHrisize,int iHriseat,int iCodetype,String strData);
    int PrintDiskbmpfile(String strPath);
    int PrintDiskimgfile(String strPath);
    int SetNvbmp(int iNums,String strPath);
    int PrintNvbmp(int iNvindex,int iMode);
    int PrintDataMatrix(String strData, int iSize);
    int GetProductinformation(int iFstype,String bFiddata,int iFidlen);
    int SetMarkoffsetcut(int iOffset);
    int SetMarkoffsetprint(int iOffset);
    int PrintMarkposition();
    int PrintMarkpositionPrint();
    int PrintMarkpositioncut();
    int PrintMarkcutpaper(int iMode);
    int PrintTransmit(String bCmd,int iLength);
    int SetCommandmode(int iMode);
    int GetPrintIDorName(String strIDorNAME);
    int SetAlignmentLeftRight(int iAlignment);
    int SetPagemode(int iMode,int Xrange,int Yrange);
    int SetPagestartposition(int Xdot,int Ydot);
    int SetPagedirection(int iDirection);
    int PrintPagedata();
    int PrintQrcodeII(String strData,int iLen,int iMside);

}
