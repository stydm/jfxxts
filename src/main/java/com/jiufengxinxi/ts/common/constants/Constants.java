/**
 * @className Constants
 * @description TODO
 * @author stydm
 * @date 2020/7/8 8:58
 */

package com.jiufengxinxi.ts.common.constants;

import java.text.DecimalFormat;

public final class Constants {

    public static final String FORMAT_yyMMddHHmmssSSS = "yyMMddHHmmssSSS";
    public static final String FORMAT_yyyyMMdd = "yyyyMMdd";
    public static final String FORMAT_yyyyMMddHH = "yyyyMMddHH";
    public static final String FORMAT_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String FORMAT_yyMMdd = "yyMMdd";
    public static final String FORMAT_yy_MM = "yy_MM";
    public static final String FORMAT_yyMM = "yyMM";
    public static final String FORMAT_yyyyMM = "yyyyMM";
    public static final String FORMAT_yyyy_MM = "yyyy_MM";
    public static final String FORMAT_yyMMddHH = "yyMMddHH";
    public static final String FORMAT_yyyy_MM_dd = "yyyy_MM_dd";
    public static final String FORMAT_yy = "yy";
    public static final String FORMAT_HH = "HH";
    public static final String FORMAT_dd = "dd";
    public static final String FORMAT_yyyy = "yyyy";

    public static final String FORMAT_YYMMDDHHMMSS = "yyMMddHHmmss";
    public static final String FORMAT_YYMMDDHHMM = "yyMMddHHmm";
    public static final String FORMAT_YYMMDD = "yyMMdd";
    public static final String FORMAT_YYMM = "yyMM";
    public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FORMAT_YYYY_MM_DD_HH = "yyyy-MM-dd HH";
    public static final String FORMAT_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";

    public static final String FORMAT_YYYY_MM = "yyyy-MM";
    public static final String FORMAT_HH_MM = "HH:mm";
    public static final String FORMAT_HH_MM_SS = "HH:mm:ss";
    public static final String FORMAT_yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_yyyy_MM_dd_HH_mm_ss_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String FORMAT_yyyy_MM_ddTHH_mm_ss_SSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String FORMAT_yyyy_MM_ddTHH = "yyyy-MM-dd'T'HH";


    public static DecimalFormat decimalFormat = new DecimalFormat("#.000000");

    public static final String ANALYSE_DATE = "analyseDate";
    public static final String BATCH_CNT = "batchCnt";

}
