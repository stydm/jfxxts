package com.jiufengxinxi.ts.common.utils.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;


public class ExecutorServiceUtils {

	//public final static ExecutorService BAS_EXECUTOR_SERVICE=new ExecutorService();
	//Executors.newSingleThreadExecutor()
	
	//private static Map<String,BaseThreadRunnable> executorServiceMap=new HashMap<String,BaseThreadRunnable>();
	
	public static BaseExecutorService executorService=new BaseExecutorService();
	
	/**
	 * 执行一个线程
	 * @param threadRunnable
	 */
	public static void execute(BaseThreadRunnable threadRunnable){
		executorService.execute(threadRunnable);
		/*ExecutorService excuteS=Executors.newSingleThreadExecutor();
		excuteS.execute(threadRunnable);
		excuteS.shutdown();*/
	}
	
	public static <T> T submit(Callable<T> call,long times) throws TimeoutException, InterruptedException, ExecutionException{
		return executorService.submit(call, times);
	}
	
	
	public static <T> T submit(Callable<T> call) throws TimeoutException, InterruptedException, ExecutionException{
		return submit(call,10*1000);
	}
	
	
}
