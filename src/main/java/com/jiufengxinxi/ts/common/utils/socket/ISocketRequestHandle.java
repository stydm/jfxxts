package com.jiufengxinxi.ts.common.utils.socket;


public interface ISocketRequestHandle {

	public String request(String report,String from);
	
	public boolean decodeRelation(String report);
	
	public void connection(String from);
	
	public String disconnection(String from);
	
}
