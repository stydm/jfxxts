package com.jiufengxinxi.ts.common.utils.thread;

/**
 * 内循环线程
 * @author roger
 *
 */
public abstract class LoopThreadRunnable extends BaseThreadRunnable  {

	@Override
	public void running() {
		while(endCondition()){
			loopElement();
			if(sleepTime()>0) {
				try {
					Thread.sleep(sleepTime());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 循环因子
	 */
	public abstract void loopElement();
	
	/**
	 * 休眠时间(毫秒)
	 * @return
	 */
	public abstract int sleepTime();
	
	/**
	 * 结束条件
	 * @return  true继续，false结束
	 */
	public abstract boolean endCondition();
}
