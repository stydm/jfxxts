package com.jiufengxinxi.ts.common.utils.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 基本线程
 * @author roger
 *
 */
public abstract class BaseThreadRunnable implements Runnable {
	
	private Logger logger= LoggerFactory.getLogger(this.getClass());
	
	private ReentrantLock reentrantLock=null;

	/**
	 * 是否需要上锁
	 * @return
	 */
	public abstract boolean isUseLock();
	
	/**
	 * 线和操作名称
	 * @return
	 */
	public abstract String getName();
	
	
	/**
	 * 运行内容
	 */
	public abstract void running();

	@Override
	public void run() {
		try{
			if(isUseLock()){
				reentrantLock=new ReentrantLock();
				reentrantLock.lock();
			}
			running();
		}catch (Exception e) {
			//e.printStackTrace();
			logger.error(getName()+"的线程运行失败:"+e.getLocalizedMessage(),e);
		}finally{
			if(isUseLock()){
				reentrantLock.unlock();
			}
		}
		
	}
	
}
