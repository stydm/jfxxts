package com.jiufengxinxi.ts.common.utils.printer;

import com.jiufengxinxi.ts.common.utils.ImageUtil;
import com.jiufengxinxi.ts.device.equipment.printer.EscPosAdapter;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;


public class UsbPrinterUtils {
	  
    
    private static Map<String, ConnectUsbEqu> printerMap = new HashMap<>();
	
	static Font font = null;
	public Font loadFont(String fontFileName, float fontSize)  //第一个参数是外部字体名，第二个是字体大小
	{
	try{
		File file = new File(fontFileName);
		FileInputStream aixing = new FileInputStream(file);
		Font dynamicFont = Font.createFont(Font.TRUETYPE_FONT, aixing);
		Font dynamicFontPt = dynamicFont.deriveFont(fontSize);
		aixing.close();
		return dynamicFontPt;
	}catch(Exception e)//异常处理
	{
		e.printStackTrace();
		return new Font("宋体", Font.PLAIN, 33);
	}
	}
	public static Font getFont(){

		if(font == null) {
			String root=System.getProperty("user.dir");//项目根目录路径
			
			font = new UsbPrinterUtils().loadFont("/Users/heshuangfeng/Documents/PycharmProjects/jfxx-printer/PingFang.ttf", 33f);//调用

			return font;//返回字体
		}
		return font;
	}
	
	/**
	 *
	 * @param printerArray
	 * @return
	 * @throws Exception 
	 */

	/**
	 *  打印
	 * @param printerArray 打印的文本
	 * @param no 二维码内容
	 * @param printSum 打印次数
	 * @return
	 * @throws Exception
	 */
	public static boolean startPrint(String printerID,short pid,short vid,String[] printerArray,String no,int printSum) throws Exception {
        try {
        	ConnectUsbEqu connectUsbEqu  = null;
        	if(printerMap.containsKey(printerID) == true) {
        		connectUsbEqu = printerMap.get(printerID);
        	}else {
        		connectUsbEqu = new ConnectUsbEqu(pid, vid);
        		printerMap.put(printerID, connectUsbEqu);
        	}
        	
       	 	Font f = getFont();  
            BufferedImage printImage = ImageUtil.createImage(printerArray, f, null, 710, 960,no);
           for (int i= 0; i<printSum;i++) {
               //打印图片
        	  boolean res =  connectUsbEqu.sendMassge(PicFromPrintUtils.draw2PxPoint2(printImage));
        	   //切纸
        	  connectUsbEqu.sendMassge(EscPosAdapter.feedPaperCutPartial());
        	  if(res == false) {
        		  connectUsbEqu = new ConnectUsbEqu(pid, vid);
        		  printerMap.remove(printerID);
        		  printerMap.put(printerID, connectUsbEqu);
        		  return false;
        	  }
            
           }
           Thread.sleep(500);
            return true;
        }catch (Exception e){
           throw e;
        }
		
	}
     public static void main(String[] args) {
    	 
    	   String printerText = "No.11113140&&&&类型:驳船作业&&&&车号:ZX890&&&&船名:欢乐&&&&堆位:51场&&&&出库单号:BCK041804934&&&&地磅单号:BDB041809010&&&&客户:厦门三裕丰能源有限公司源有限公司源有限公司&&&&卡号/驳船名:粤伟航326&&&&空重（吨）:15.84&&&&毛重（吨）:37.88&&&&净重（吨）:22.04&&&&累计重量(吨):371.34    车次：15&&&&配载结余（吨）:1931.66&&&&地磅单结存（吨）：-22230.36&&&&司磅员:刘小玲&&&&作业票号:BGB041812091010&&&&日期:2018-12-10     中班&&&&过磅时间:01:49:20&&&&新沙港务有限公司 地中衡记录    (重)&&&&uuid：37ee4bb0-980a-409d-8952-6f8b63a0c1f3&&&&";
    		String[] printer = printerText.split("&&&&");
            String no = printer[0].replace("No.","");
    	   try {
			startPrint("x",(short)0x5812, (short)0x28e9,printer,no,1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
}
