package com.jiufengxinxi.ts.common.utils.thread;

import java.util.concurrent.Callable;

public abstract class CallableByParam<T extends Object> implements Callable<T> {
    private Object[] params;


    public CallableByParam(Object ... params) {
        this.params = params;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}
