package com.jiufengxinxi.ts.common.utils.printer;

import com.jiufengxinxi.ts.common.utils.ImageUtil;

import java.awt.image.BufferedImage;


/**
 * 将图片转化为二进制
 * @author nsz
 * 2015年1月30日
 */
public class PicFromPrintUtils {
	

	public static byte[] draw2PxPoint2(BufferedImage bmp) {
		int size = bmp.getWidth() * bmp.getHeight() / 8 + 10000;
		byte[] data = new byte[size];
		int k = 0;
		//设置行距为0的指令
		data[k++] = 0x1B;
		data[k++] = 0x33;
		data[k++] = 0x00;
		// 逐行打印
		for (int j = 0; j < bmp.getHeight() / 24f; j++) {
			//打印图片的指令
			data[k++] = 0x1B;
			data[k++] = 0x2A;
			data[k++] = 33;
			data[k++] = (byte) (bmp.getWidth() % 256); //nL
			data[k++] = (byte) (bmp.getWidth() / 256); //nH
			//对于每一行，逐列打印
			for (int i = 0; i < bmp.getWidth(); i++) {
				//每一列24个像素点，分为3个字节存储
				for (int m = 0; m < 3; m++) {
					//每个字节表示8个像素点，0表示白色，1表示黑色
					for (int n = 0; n < 8; n++) {
						byte b =0 ;
						b = ImageUtil.px2Byte(i, j * 24 + m * 8 + n, bmp);
						data[k] += data[k] + b;
						
					}
					k++;
				}
			}
			data[k++] = 10;//换行
		}
		return data;
	}


}
