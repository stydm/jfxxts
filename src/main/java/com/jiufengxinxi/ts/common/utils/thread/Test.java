package com.jiufengxinxi.ts.common.utils.thread;

public class Test {

	public static int e=0;
	
	public static void main(String[] args){
		ExecutorServiceUtils.execute(new LoopThreadRunnable() {
			
			@Override
			public boolean isUseLock() {
				return false;
			}
			
			@Override
			public String getName() {
				return "testBusiness2";
			}
			
			@Override
			public int sleepTime() {
				return 500;
			}
			
			@Override
			public void loopElement() {
				System.out.println(getName()+"运行中，"+e+"  "+System.currentTimeMillis());
			}
			
			@Override
			public boolean endCondition() {
				return e++!=10;
			}
		});
		
		
		
		ExecutorServiceUtils.execute(new BaseThreadRunnable() {
			
			@Override
			public void running() {
				System.out.println(getName()+"运行中，"+System.currentTimeMillis());
			}
			
			@Override
			public boolean isUseLock() {
				return false;
			}
			
			@Override
			public String getName() {
				return "testBusiness";
			}
		});
		
		
	}
	
}
