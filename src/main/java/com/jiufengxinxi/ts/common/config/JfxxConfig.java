package com.jiufengxinxi.ts.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author stydm
 * @className JfxxConfig
 * @description 玖峰信息相关配置
 * @date 2020/7/8 17:04
 */

@Configuration
@ConfigurationProperties(prefix = "jfxx")
@Data
public class JfxxConfig {
    private String username;
    private String password;
    private String terminalKey;
    private String terminalVersion;
}
