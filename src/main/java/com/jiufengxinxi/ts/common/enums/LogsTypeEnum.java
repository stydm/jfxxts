package com.jiufengxinxi.ts.common.enums;

/**
 * 
 * 响应类型枚举
 * 
 */
public enum LogsTypeEnum {
	SYSTEM_OK("1"),     // 系统日志
	BUSINESS_OK("2"),   // 业务日志
	OPERATION_OK("3"),  //运维日志
	OPERATION_NO("9"),  //运维异常日志
	BUSINESS_NO("4"),   // 业务异常日志
	SYSTEM_NO("5"),     // 系统异常日志
	EVENT_OK("6"),      // 事件日志
	DEVIDE_OK("7"),
	DEVICE_NO("8");

	private String value;

	private LogsTypeEnum(String value) {

		this.value = value;

	}

	public String getValue() {

		return this.value;

	}
}
