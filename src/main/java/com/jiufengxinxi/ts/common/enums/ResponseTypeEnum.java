package com.jiufengxinxi.ts.common.enums;

/**
 * 
 * 响应类型枚举
 *
 */
public enum ResponseTypeEnum {
	
	/**
	 * 成功
	 */
	SUCCESS("SUCCESS","成功"),
	
	/**
	 * 失败
	 */
	FAILD("FAILD","失败"),
	OUT_LOGIN("OUT_LOGIN","登录失效"),
	NO_ADAPTER("NO_ADAPTER","无对应业务"),
	EXCEPTION("EXCEPTION","业务异常"),
	
	ANALYSIS_ERROR("ANALYSIS_ERROR","解析异常");
	
	private String name;
	private String desc;

	private ResponseTypeEnum(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}
	
	 public static ResponseTypeEnum toEnum(String responseCodeEnum) {
	        try {
	            return Enum.valueOf(ResponseTypeEnum.class, responseCodeEnum);
	        } catch(Exception ex) {
	            for(ResponseTypeEnum period : ResponseTypeEnum.values()) {
	                if(period.getName().equalsIgnoreCase(responseCodeEnum)) {
	                    return period;
	                }
	            }
	            throw new IllegalArgumentException("Cannot convert <" + responseCodeEnum + "> to responseCodeEnum enum");
	        }
	 }

}
