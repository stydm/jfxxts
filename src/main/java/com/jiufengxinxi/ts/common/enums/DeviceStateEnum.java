package com.jiufengxinxi.ts.common.enums;

public enum DeviceStateEnum {

    CONNECTED("0","已连接"),
    DISCONNECTED("1","已断开"),
    GOOD("2","良好"),
    ERROR("3","故障"),
    PRINTER_1("4","卡纸"),
    PRINTER_2("5","缺纸");


    private String value;

    private String desc;

    DeviceStateEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
