package com.jiufengxinxi.ts.common.enums;

/**
 * 
 * 响应类型枚举
 * 
 */
public enum BusinessLogsTypeEnum {
	BDATA("0","BUSINESS_DATA","业务","业务数据"),
	TRIGGER("1","TRIGGER","触发器","触发"),
	READER("2","READER","读写器","读卡"),
	CARNUM("3","CARNUM","业务","车牌获取"),
	WEIGHT("4","WEIGHT","地磅","稳定重量"),
	BPRODUCE("5","BUSINESS","业务","业务处理完成"),
	OPENGATE("6","OPENGATE","道闸","开闸"),
	FLOWOVER("7","BUSINESS_DONE","业务","流程结束"),
	NOPASS("8","BUSINESS_NOPASS","业务","业务不通过"),
	EXCEPTION("9","BUSINESS_EXCEPTION","业务","业务异常"),
	INTERRUPT("10","BUSINESS_INTERRUPT","业务","处理异常中断"),
	NOPRODUCE("11","BUSINESS_NOPRO","业务","业务未处理"),
	READGCARD("12","READ_GOODS_CARD","读货卡","读到货卡"),
	BUSINESS_START("13","BUSINESS_START","业务开始","车辆进入");

	private String value;

	private String desc;

	private String objectcode;

	private String object;

	private BusinessLogsTypeEnum(String value,String objectcode,String object,String desc) {

		this.value = value;
		this.desc = desc;
		this.objectcode = objectcode;
		this.object = object;
	}

	public String getValue() {

		return this.value;

	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getObjectcode() {
		return objectcode;
	}

	public void setObjectcode(String objectcode) {
		this.objectcode = objectcode;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}


    @Override
    public String toString() {
        return "业务编号:"+value+",对象编码:"+objectcode+",对象:"+object+",说明:"+desc;
    }}
