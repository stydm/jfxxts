package com.jiufengxinxi.ts.common.enums;

public enum ProtocolTypeEnum {
    NONE(0,"无"),
    HTTP(1,"HTTP协议"),
    SOCKET(2,"SOCKET协议"),
    COMM(3,"串口"),
    RTSP(4,"RTSP协议"),
    API(5,"API"),
    ONVIF(6,"ONVIF"),
    USB(7,"USB")
    ;


    private int value;

    private String desc;

    ProtocolTypeEnum(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
