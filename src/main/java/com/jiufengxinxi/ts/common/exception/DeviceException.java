package com.jiufengxinxi.ts.common.exception;

public class DeviceException extends BusinessException {

	private String deviceCode;
	
	private String deviceName;
	
	private String site;
	
	private String desc;
	
	
	public DeviceException(String userMsg, Exception source) {
		super(userMsg, source);
	}
	
	public DeviceException(String deviceCode, String deviceName, Exception source) {
		super(source);
		this.deviceCode = deviceCode;
		this.deviceName = deviceName;
		setUserMsg(deviceName+"出现异常");
	}
	

	public DeviceException(String deviceCode, String deviceName, String site,
                           String desc, Exception source) {
		super(desc, source);
		this.deviceCode = deviceCode;
		this.deviceName = deviceName;
		this.site = site;
		this.desc = desc;
	}

	public String getDeviceCode() {
		return deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	
}
