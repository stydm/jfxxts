
package com.jiufengxinxi.ts.common.exception;

/**
 * 公共异常类
 * @author cjh
 */
@SuppressWarnings("serial")
public class BusinessException extends RuntimeException {
    private String userMsg = "";
    private Object userData = null;
    
    public BusinessException() {
        super();
    }
	public BusinessException(String message) {
		super(message);
	}
	
    public BusinessException(Exception source) {
        super();
        this.setStackTrace(source.getStackTrace());
        this.initCause(source);
    }
    
    public BusinessException(String userMsg, Exception source) {
        super();
        this.setStackTrace(source.getStackTrace());
        this.initCause(source);
        this.setUserMsg(userMsg);
    }

    /**
     *获取用户消息
     */
    public String getUserMsg() {
        return userMsg;
    }

    /**
     *设置用户消息
     */
    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }
    
    @Override
    public String toString() {
        String s = super.toString();
        if(this.userMsg == null || this.userMsg.length() == 0)
            return s;
        
        return this.userMsg+"\n "+this.getMessage() + System.getProperty("line.separator") + s;
    }

	public Object getUserData() {
		return userData;
	}

	public void setUserData(Object userData) {
		this.userData = userData;
	}
}
