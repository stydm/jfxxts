package com.jiufengxinxi.ts.common.exception;

/**
 * 资金业务异常
 * @author roger
 *
 */
public class FundBussinessException extends BusinessException {

	private String code;

	public FundBussinessException() {
		super();
	}

	public FundBussinessException(Exception source) {
		super(source);
	}

	public FundBussinessException(String userMsg, Exception source) {
		super(userMsg, source);
	}

	public FundBussinessException(String message) {
		super(message);
	}
	
	public FundBussinessException(String code,String message) {
		super(message);
		this.code=code;
	}
	
}
