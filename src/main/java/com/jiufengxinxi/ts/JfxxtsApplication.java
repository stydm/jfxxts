package com.jiufengxinxi.ts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JfxxtsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JfxxtsApplication.class, args);
	}

}
